server {
    
    listen 99;
    port_in_redirect off;
	index index.html;
    
    server_name staging.treebohotels.com catalog.treebo.be; 
    server_tokens off;
    add_header X-Frame-Options SAMEORIGIN;
    add_header X-Content-Type-Options nosniff;
    add_header X-XSS-Protection "1; mode=block";
    access_log "/var/log/nginx/staging.access.log";
    error_log "/var/log/nginx/staging.error.log";


    location /its/admin {
        proxy_pass http://localhost:9085;
    }
	

    location /corporate_bookings {
        proxy_pass http://localhost:10101;
    }

    location /corporate_bookings/static {
        alias /ebs1/corporate-bookings/webapp/static_pipeline;
    }

    location /prowl/static {
            alias /automation/prowl/prowl/webapp/static;
    }
   
    location /treeboauth/static {
            alias /home/jenkins/static;
    } 
    location /favicon.ico { alias /usr/share/ntop/html/favicon.ico; }
  
    location /static {
        autoindex on;
	    alias /static;
        allow all;
        gzip_http_version 1.0;
        gzip_static  on;
        add_header   Last-Modified "";
        add_header   Cache-Control public;
        if ($request_filename ~* ^.*?/([^/]*?)$)
        {
                set $fname $1;
        }

        if ($fname ~* ^.*?\.(eot)|(ttf)|(ttc)|(otf)|(woff)|(woff2)$){
                add_header   Access-Control-Allow-Origin *;
        }
        expires     max;
    }

    location /dist {
        alias   /automation/treebo/webapp/dist/;
        allow all;
        gzip_http_version 1.0;
        gzip_static  on;
        add_header   Last-Modified "";
        add_header   Cache-Control public;
        if ($request_filename ~* ^.*?/([^/]*?)$)
        {
                set $fname $1;
        }

        if ($fname ~* ^.*?\.(eot)|(ttf)|(ttc)|(otf)|(woff|woff2)$){
                add_header   Access-Control-Allow-Origin *;
        }
        expires     max;
    }
	
    location /blog {
        set $request_url "";
        if ($request_uri ~ ^/blog/(.*)$) {
                set $request_url $1;
        }
        return 302 http://blog.treebohotels.com/$request_url;
    }
    location /admin {
            proxy_pass http://testing.treebohotels.com/404;
    }
    
    location /growth {
	proxy_pass http://localhost:9053;
    }

    location /booking_exp{
		proxy_pass http://localhost:9053;
	}
    location /otp  {
       	        proxy_pass http://localhost:8050;
    }
    location /api/v1/growth {
                proxy_pass http://localhost:9053;
    }

set $mobile_redirect do_not_perform;

	if ($http_user_agent ~* "(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino") {
  		set $mobile_redirect perform;
	}

	if ($http_user_agent ~* "^(1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-)") {
  		set $mobile_redirect perform;
	}

  	if ($args ~ 'mobile=true') {
    		set $mobile_redirect perform;
    		set $mobile_cookie  "mobile=true";
  	}

  	if ($args ~ 'mobile=false') {
    		set $mobile_cookie  "mobile=false";
  	}

  	add_header Set-Cookie $mobile_cookie;

  	if ($http_cookie ~ 'mobile=true') {
    		set $mobile_redirect perform;
  	}

	set $ismobile 0;
	if ( $mobile_redirect = perform){
    		set $ismobile 1;
	}

	location  /bumblebee/ {
    		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    		proxy_set_header Host $http_host;
    		proxy_redirect off;
    		proxy_connect_timeout       7200;
    		proxy_send_timeout          7200;
    		keepalive_timeout           7200;
    		send_timeout                7200;
    		if (!-f $request_filename) {
            		proxy_pass http://localhost:8008;
            		break;
    		}
	}

    location /migration/ {
	proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	proxy_set_header Host $http_host;
	proxy_redirect off;
	proxy_connect_timeout       7200;
	proxy_send_timeout          7200;
	proxy_read_timeout          7200;
	send_timeout                7200;
	if (!-f $request_filename) {
		proxy_pass http://localhost:8090;
	}
    }

	location  /bb_cm/ {
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $http_host;
                proxy_redirect off;
                proxy_connect_timeout       7200;
                proxy_send_timeout          7200;
                proxy_read_timeout          7200;
                send_timeout                7200;
                if (!-f $request_filename) {
                        proxy_pass http://localhost:8032;
                        break;
                }
        }

        location  /cm/ {
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $http_host;
                proxy_redirect off;
                proxy_connect_timeout       7200;
                proxy_send_timeout          7200;
                proxy_read_timeout          7200;
                send_timeout                7200;
                if (!-f $request_filename) {
                        proxy_pass http://localhost:8005;
                        break;
                }
        }

	location  /treeboauth/ {
    		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    		proxy_set_header Host $http_host;
    		proxy_redirect off;
    		proxy_connect_timeout       7200;
    		proxy_send_timeout          7200;
    		proxy_read_timeout          7200;
    		send_timeout                7200;
    		if (!-f $request_filename) {
            		proxy_pass http://localhost:8051;
            		break;
    		}
	}

    location  /lsdauth/ {
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $http_host;
            proxy_redirect off;
            proxy_connect_timeout       7200;
            proxy_send_timeout          7200;
            proxy_read_timeout          7200;
            send_timeout                7200;
            if (!-f $request_filename) {
                    proxy_pass http://localhost:8041;
                    break;
            }
    }

	location  /prowl/ {
    		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    		proxy_set_header Host $http_host;
    		proxy_redirect off;
    		proxy_connect_timeout       7200;
    		proxy_send_timeout          7200;
    		proxy_read_timeout          7200;
    		send_timeout                7200;
    		if (!-f $request_filename) {
            		proxy_pass http://localhost:8002;
            		break;
    		}
    }
	location  /stay/ {
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $http_host;
                proxy_redirect off;
                proxy_connect_timeout       7200;
                proxy_send_timeout          7200;
                proxy_read_timeout          7200;
                send_timeout                7200;
                if (!-f $request_filename) {
                        proxy_pass http://localhost:9001;
                        break;
                }
    	}

	location  /hmssync/ {
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $http_host;
                proxy_redirect off;
                proxy_connect_timeout       7200;
                proxy_send_timeout          7200;
                proxy_read_timeout          7200;
                send_timeout                7200;
                if (!-f $request_filename) {
                        proxy_pass http://localhost:8070;
                        break;
                }
    }
        
    location  /lsd/ {
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $http_host;
                proxy_redirect off;
                proxy_connect_timeout       7200;
                proxy_send_timeout          7200;
                proxy_read_timeout          7200;
                send_timeout                7200;
                if (!-f $request_filename) {
                        proxy_pass http://localhost:9011;
                        break;
                }
    }
	
	location  /_nested_admin/ {
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $http_host;
                proxy_redirect off;
                proxy_connect_timeout       7200;
                proxy_send_timeout          7200;
                proxy_read_timeout          7200;
                send_timeout                7200;
                if (!-f $request_filename) {
                        proxy_pass http://localhost:9001;
                        break;
                }
    }

    location  /cataloging-service/ {
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $http_host;
                client_max_body_size 200M;
                proxy_redirect off;
                proxy_connect_timeout       7200;
                proxy_send_timeout          7200;
                proxy_read_timeout          7200;
                send_timeout                7200;
                if (!-f $request_filename) {
                        proxy_pass http://localhost:8000;
                        break;
                }
    }        

 	location /rabbitmq/ {
                if ($request_uri ~* "/rabbitmq/(.*)") {
                        proxy_pass http://127.0.0.1:15672/$1;
                }
    }
   

        location ~ ^/(pages|booking|checkout|rest)/ {
            if ($request_method = OPTIONS) {
                 add_header Access-Control-Allow-Origin *;
                 # add_header Access-Control-Allow-Methods "GET, POST, PUT, PATCH, OPTIONS";
                 add_header 'Access-Control-Allow-Headers' 'Authorization,Content-Type,Accept,Origin,User-Agent,DNT,Cache-Control,X-Mx-ReqToken,Keep-Alive,X-Requested-With,If-Modified-Since';
                 # add_header Access-Control-Allow-Credentials true;
                 return 200;
            }
                 add_header Access-Control-Allow-Origin *;
                 # add_header Access-Control-Allow-Methods "GET, POST, PUT, PATCH, OPTIONS";
                 add_header 'Access-Control-Allow-Headers' 'Authorization,Content-Type,Accept,Origin,User-Agent,DNT,Cache-Control,X-Mx-ReqToken,Keep-Alive,X-Requested-With,If-Modified-Since';
                 # add_header Access-Control-Allow-Credentials true;
             	proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
              	proxy_set_header Host $http_host;
              	proxy_set_header ISMOBILE $ismobile;
              	proxy_redirect off;
              	proxy_connect_timeout       7200;
              	proxy_send_timeout          7200;
              	proxy_read_timeout          7200;
              	send_timeout                7200;
              	if (!-f $request_filename) {
                  	set $optproxy A;
              	}
    	     	if ( $mobile_redirect = perform) {
            		set $optproxy "${optproxy}B";
                }
    		if ( $optproxy = AB ){
        		proxy_pass http://localhost:8020;
        		break;
    		}
    		if ( $optproxy = A ){
            		proxy_pass http://localhost:8020;
            		break;
    		}
        }
 
 #   location /api {
 #           
 #         proxy_pass http://localhost:8001;
                
 #   }

    location / {
      proxy_pass http://localhost:7001;
    }

   location /packages {
      alias  /home/amith.soman/pypi/packages;
   }   
   
   location /build {
          alias /home/ubuntu/hotrod/build;
   }  
 
   location /api/treeboauth/admin {
            if ($request_uri = /api/treeboauth/admin) {
#          proxy_set_header X-Real-IP $remote_addr;
#          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          rewrite ^ /pms/v1/rates;
         #proxy_pass http://pricing.treebohotels.com;
          break;
#          proxy_redirect off;
#          return 200;
        }
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $http_host;
            proxy_redirect off;
            proxy_connect_timeout       7200;
            proxy_send_timeout          7200;
            proxy_read_timeout          7200;
            send_timeout                7200;
	   #return 302 $scheme://staging.treebohotels.com/treeboauth/admin;
 }

    location /alpha {
	return 301 http://alpha.treebo.com;
    }

    location /offers {
     	return 301 http://offers.treebo.com;
    }

    location /media/ {
        # if asset versioning is used
        if ($query_string) {
            expires 1w;
        }
    }
}

