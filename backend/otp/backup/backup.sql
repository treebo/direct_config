--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.7
-- Dumped by pg_dump version 10.1

-- Started on 2018-01-09 19:33:08 IST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 173 (class 1259 OID 284893)
-- Name: alembic_version; Type: TABLE; Schema: public; Owner: loadtesting
--

CREATE TABLE alembic_version (
    version_num character varying(32) NOT NULL
);


ALTER TABLE alembic_version OWNER TO loadtesting;

--
-- TOC entry 175 (class 1259 OID 284900)
-- Name: otp_config; Type: TABLE; Schema: public; Owner: loadtesting
--

CREATE TABLE otp_config (
    created_at timestamp with time zone,
    modified_at timestamp with time zone,
    id integer NOT NULL,
    config_type character varying(50) NOT NULL,
    ttl integer,
    text character varying(500) NOT NULL
);


ALTER TABLE otp_config OWNER TO loadtesting;

--
-- TOC entry 174 (class 1259 OID 284898)
-- Name: otp_config_id_seq; Type: SEQUENCE; Schema: public; Owner: loadtesting
--

CREATE SEQUENCE otp_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE otp_config_id_seq OWNER TO loadtesting;

--
-- TOC entry 2889 (class 0 OID 0)
-- Dependencies: 174
-- Name: otp_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: loadtesting
--

ALTER SEQUENCE otp_config_id_seq OWNED BY otp_config.id;


--
-- TOC entry 177 (class 1259 OID 284913)
-- Name: otp_verification; Type: TABLE; Schema: public; Owner: loadtesting
--

CREATE TABLE otp_verification (
    created_at timestamp with time zone,
    modified_at timestamp with time zone,
    id integer NOT NULL,
    phone_number character varying(20) NOT NULL,
    verification_code character varying(30) NOT NULL,
    identifier character varying(200) NOT NULL,
    attempts integer NOT NULL,
    is_active boolean NOT NULL,
    expiry timestamp without time zone NOT NULL,
    sms_sent_time timestamp without time zone,
    sms_delivered_time timestamp without time zone,
    sms_verified_time timestamp without time zone,
    sms_status character varying(50) NOT NULL,
    notification_id character varying(50),
    template_payload json
);


ALTER TABLE otp_verification OWNER TO loadtesting;

--
-- TOC entry 176 (class 1259 OID 284911)
-- Name: otp_verification_id_seq; Type: SEQUENCE; Schema: public; Owner: loadtesting
--

CREATE SEQUENCE otp_verification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE otp_verification_id_seq OWNER TO loadtesting;

--
-- TOC entry 2890 (class 0 OID 0)
-- Dependencies: 176
-- Name: otp_verification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: loadtesting
--

ALTER SEQUENCE otp_verification_id_seq OWNED BY otp_verification.id;


--
-- TOC entry 2759 (class 2604 OID 284903)
-- Name: otp_config id; Type: DEFAULT; Schema: public; Owner: loadtesting
--

ALTER TABLE ONLY otp_config ALTER COLUMN id SET DEFAULT nextval('otp_config_id_seq'::regclass);


--
-- TOC entry 2760 (class 2604 OID 284916)
-- Name: otp_verification id; Type: DEFAULT; Schema: public; Owner: loadtesting
--

ALTER TABLE ONLY otp_verification ALTER COLUMN id SET DEFAULT nextval('otp_verification_id_seq'::regclass);


--
-- TOC entry 2880 (class 0 OID 284893)
-- Dependencies: 173
-- Data for Name: alembic_version; Type: TABLE DATA; Schema: public; Owner: loadtesting
--

COPY alembic_version (version_num) FROM stdin;
df59e388f61b
\.


--
-- TOC entry 2882 (class 0 OID 284900)
-- Dependencies: 175
-- Data for Name: otp_config; Type: TABLE DATA; Schema: public; Owner: loadtesting
--

COPY otp_config (created_at, modified_at, id, config_type, ttl, text) FROM stdin;
2017-05-24 14:50:29.929+00	2017-05-29 04:37:46.993149+00	1	web_template	300	Dear {first_name} , Your OTP for booking a Treebo is {verification_code}. Thank you! Treebo Hotels.
2017-06-07 11:40:55+00	2017-06-07 11:55:30+00	4	Direct	60	Dear {last_name} {team} , your {verification_code} , Thanks Team Treebo.
2017-06-07 09:50:55.89+00	2017-06-07 09:53:28.691976+00	5	Test	60	Dear {last_name} {team} , your {verification_code} , Thanks Team Treebo.
2017-06-07 09:50:55.89+00	2017-06-07 09:53:28.691976+00	3	prowl	120	Dear {first_name} {team} and  hjkj {verification_code}
2017-06-04 10:14:22.96+00	2017-06-07 18:27:16.377073+00	2	default	1000	{verification_code} is your OTP for Treebo.
\.


--
-- TOC entry 2891 (class 0 OID 0)
-- Dependencies: 174
-- Name: otp_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: loadtesting
--

SELECT pg_catalog.setval('otp_config_id_seq', 1, false);


--
-- TOC entry 2892 (class 0 OID 0)
-- Dependencies: 176
-- Name: otp_verification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: loadtesting
--

SELECT pg_catalog.setval('otp_verification_id_seq', 2195, true);


--
-- TOC entry 2768 (class 2606 OID 284923)
-- Name: otp_verification _unique_identifier; Type: CONSTRAINT; Schema: public; Owner: loadtesting
--

ALTER TABLE ONLY otp_verification
    ADD CONSTRAINT _unique_identifier UNIQUE (phone_number, identifier, verification_code);


--
-- TOC entry 2762 (class 2606 OID 284897)
-- Name: alembic_version alembic_version_pkc; Type: CONSTRAINT; Schema: public; Owner: loadtesting
--

ALTER TABLE ONLY alembic_version
    ADD CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num);


--
-- TOC entry 2764 (class 2606 OID 284910)
-- Name: otp_config otp_config_config_type_key; Type: CONSTRAINT; Schema: public; Owner: loadtesting
--

ALTER TABLE ONLY otp_config
    ADD CONSTRAINT otp_config_config_type_key UNIQUE (config_type);


--
-- TOC entry 2766 (class 2606 OID 284908)
-- Name: otp_config otp_config_pkey; Type: CONSTRAINT; Schema: public; Owner: loadtesting
--

ALTER TABLE ONLY otp_config
    ADD CONSTRAINT otp_config_pkey PRIMARY KEY (id);


--
-- TOC entry 2770 (class 2606 OID 284921)
-- Name: otp_verification otp_verification_pkey; Type: CONSTRAINT; Schema: public; Owner: loadtesting
--

ALTER TABLE ONLY otp_verification
    ADD CONSTRAINT otp_verification_pkey PRIMARY KEY (id);


-- Completed on 2018-01-09 19:33:19 IST

--
-- PostgreSQL database dump complete
--

