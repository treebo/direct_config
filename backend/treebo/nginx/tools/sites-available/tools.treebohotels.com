server {
    listen 80;
    server_name tools.treebohotels.com;
    access_log /var/log/nginx/access_json.log le_json;
    access_log /var/log/nginx/error_json.log le_json;

    root html;
    index index.html index.htm;

    location /corporate_bookings {
        proxy_set_header    Host                $host;
        proxy_set_header    X-Real-IP           $remote_addr;
        proxy_set_header    X-Forwarded-For     $remote_addr;
        proxy_set_header    X-Originating-IP    $remote_addr;
        proxy_set_header    HTTP_REMOTE_ADDR    $remote_addr;
        proxy_set_header    REMOTE_ADDR         $remote_addr;
        proxy_set_header    CLIENT_IP           $remote_addr;
        proxy_pass http://127.0.0.1:10101/corporate_bookings;
    }

    location  /blosdg {
                set $request_url "";
                if ($request_uri ~ ^/blog/(.*)$) {
                set $request_url $1;
        }
                proxy_set_header Host $host;
                proxy_pass         http://127.0.0.1:8081/blog/$request_url;
        }

    location /treebune {
        proxy_set_header   X-Real-IP $remote_addr;
        proxy_set_header   Host      $http_host;
        proxy_pass         http://127.0.0.1:2368;
    }

    location /corporate_bookings/static {
	alias /ebs1/corporate-bookings/webapp/static_pipeline;
    }
    location /bulkMailer {
        proxy_pass http://127.0.0.1:5003/bulkMailer;
    }
    location /uploadprice {
        proxy_pass http://127.0.0.1:8000/uploadprice;
    }
    location /admin {
        #    limit_req zone=treebo_ten nodelay;
	client_max_body_size 4000M;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_redirect off;
        proxy_connect_timeout       7200;
        proxy_send_timeout          7200;
        proxy_read_timeout          7200;
        send_timeout                7200;
        if (!-f $request_filename) {
            proxy_pass http://localhost:8020;
            break;
        }
    }

    location /badas {
        proxy_pass http://127.0.0.1:8022/badas;
    }
    location /bookingsearch {
        proxy_pass http://127.0.0.1:10002/;
    }

    location /bookingsearch/static {
        #proxy_pass http://127.0.0.1:10002/;
	alias /home/arun.midha/arun_hack/hms_searching/static;
    }
    location /hotelmodule {
        proxy_pass http://127.0.0.1:9021/hotelmodule;
    }

    location /badas/api/ {
        proxy_pass http://127.0.0.1:8021/badas/;
    }

    location /badas/admin {
	proxy_pass http://127.0.0.1:8021/badas/admin;
    }

    location /badas/build/ {
	alias /ebs1/badas_client/build/;
	allow all;
	gzip_http_version 1.0;
	gzip_static  on;
	add_header   Last-Modified "";
	add_header   Cache-Control public;
	if ($request_filename ~* ^.*?/([^/]*?)$)
	{
	set $fname $1;
	}

	if ($fname ~* ^.*?\.(eot)|(ttf)|(ttc)|(otf)|(woff|woff2)$){
	add_header   Access-Control-Allow-Origin *;
	}
	expires     max;
    }


    location /promos-admin {
                #proxy_pass http://nikhil.rajawat.treebohotel.in/admin;
                proxy_pass http://localhost:9009;
    }




    location /fot/admin {
                #proxy_pass http://nikhil.rajawat.treebohotel.in/admin;
		auth_basic "Restricted Content";
		auth_basic_user_file /etc/nginx/.htpasswd;
                proxy_pass http://localhost:10001;
    }
    location /fot/api {
    		proxy_pass https://www.treebohotels.com;
    }

    location /api/v1/checkout/booking/cancel/ {
		proxy_pass https://www.treebohotels.com;
    }
    location /static {
                #autoindex on;
                alias   /home/nikhil.rajawat/treebo_internal/treebo_internal/static_pipeline/;

                allow all;
                gzip_http_version 1.0;
                gzip_static  on;
                add_header   Last-Modified "";
                add_header   Cache-Control public;
                if ($request_filename ~* ^.*?/([^/]*?)$)
                {
                        set $fname $1;
                }

                if ($fname ~* ^.*?\.(eot)|(ttf)|(ttc)|(otf)|(woff)|(woff2)$){
                        add_header   Access-Control-Allow-Origin *;
                }
                expires     max;
        }

    location /badas/static {
                #autoindex on;
                alias   /ebs1/badas/static_pipeline;

                allow all;
                gzip_http_version 1.0;
                gzip_static  on;
                add_header   Last-Modified "";
                add_header   Cache-Control public;
                if ($request_filename ~* ^.*?/([^/]*?)$)
                {
                        set $fname $1;
                }

                if ($fname ~* ^.*?\.(eot)|(ttf)|(ttc)|(otf)|(woff)|(woff2)$){
                        add_header   Access-Control-Allow-Origin *;
                }
                expires     max;
        }

    location /hotelmodule/static {
                #autoindex on;
                alias   /ebs1/hotelmodule/static_pipeline;

                allow all;
                gzip_http_version 1.0;
                gzip_static  on;
                add_header   Last-Modified "";
                add_header   Cache-Control public;
                if ($request_filename ~* ^.*?/([^/]*?)$)
                {
                        set $fname $1;
                }

                if ($fname ~* ^.*?\.(eot)|(ttf)|(ttc)|(otf)|(woff)|(woff2)$){
                        add_header   Access-Control-Allow-Origin *;
                }
                expires     max;
        }



    location /dist {
                #autoindex on;
                alias   /ebs1/treebo-fot/webapp/dist/;

                allow all;
                gzip_http_version 1.0;
                gzip_static  on;
                add_header   Last-Modified "";
                add_header   Cache-Control public;
                if ($request_filename ~* ^.*?/([^/]*?)$)
                {
                        set $fname $1;
                }

                if ($fname ~* ^.*?\.(eot)|(ttf)|(ttc)|(otf)|(woff)|(woff2)$){
                        add_header   Access-Control-Allow-Origin *;
                }
                expires     max;
        }


    location / {
        proxy_set_header    Host                $host;
        proxy_set_header    X-Real-IP           $remote_addr;
        proxy_set_header    X-Forwarded-For     $remote_addr;
        proxy_set_header    X-Originating-IP    $remote_addr;
        proxy_set_header    HTTP_REMOTE_ADDR    $remote_addr;
        proxy_set_header    REMOTE_ADDR         $remote_addr;
        proxy_set_header    CLIENT_IP           $remote_addr;
        proxy_pass http://127.0.0.1:8020/;
    }

    location /upload {
        proxy_set_header    Host                $host;
        proxy_set_header    X-Real-IP           $remote_addr;
        proxy_set_header    X-Forwarded-For     $remote_addr;
        proxy_set_header    X-Originating-IP    $remote_addr;
        proxy_set_header    HTTP_REMOTE_ADDR    $remote_addr;
        proxy_set_header    REMOTE_ADDR         $remote_addr;
        proxy_set_header    CLIENT_IP           $remote_addr;
        proxy_pass http://127.0.0.1:9009;
    }


    location /promos {
	if (-d $request_filename) {
 	   rewrite [^/]$ $scheme://$http_host$uri/ permanent;
	}
        proxy_set_header    Host                $host;
        proxy_set_header    X-Real-IP           $remote_addr;
        proxy_set_header    X-Forwarded-For     $remote_addr;
        proxy_set_header    X-Originating-IP    $remote_addr;
        proxy_set_header    HTTP_REMOTE_ADDR    $remote_addr;
        proxy_set_header    REMOTE_ADDR         $remote_addr;
        proxy_set_header    CLIENT_IP           $remote_addr;
        proxy_pass http://127.0.0.1:9009;
    }

    location /rms {
        proxy_set_header    Host                $host;
        proxy_set_header    X-Real-IP           $remote_addr;
        proxy_set_header    X-Forwarded-For     $remote_addr;
        proxy_set_header    X-Originating-IP    $remote_addr;
        proxy_set_header    HTTP_REMOTE_ADDR    $remote_addr;
        proxy_set_header    REMOTE_ADDR         $remote_addr;
        proxy_set_header    CLIENT_IP           $remote_addr;
        proxy_pass http://127.0.0.1:9010;
    }



}
