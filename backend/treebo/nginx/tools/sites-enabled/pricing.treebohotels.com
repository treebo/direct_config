upstream pricing_service {
    keepalive   1000;
    server      127.0.0.1:7896;
}

server {
    listen 90;
    root html;
    server_name pricing.treebohotels.com api.treebohotels.com;
    index index.html index.htm;

    access_log  /var/log/nginx/pricing.access.log;
    error_log   /var/log/nginx/pricing.error.log;

    location /tax {
        proxy_pass http://127.0.0.1:7897;
    }

    location /v1 {
        proxy_http_version 1.1;
        proxy_set_header Connection "";
        proxy_set_header X-Request-Start "t=${msec}";
        keepalive_timeout 70s;
        proxy_pass http://127.0.0.1:7897;
    }
    location /pricing {
        proxy_http_version 1.1;
        proxy_set_header Connection "";
        proxy_set_header X-Request-Start "t=${msec}";
        keepalive_timeout 70s;
        proxy_pass http://127.0.0.1:7897;
    }

    location /v2 {
        proxy_http_version 1.1;
        proxy_set_header Connection "";
        proxy_set_header X-Request-Start "t=${msec}";
        keepalive_timeout 70s;
        proxy_pass http://127.0.0.1:7897;
    }

    location /v3 {
        proxy_http_version 1.1;
        proxy_set_header Connection "";
        proxy_set_header X-Request-Start "t=${msec}";
        keepalive_timeout 70s;
        proxy_pass http://127.0.0.1:7897;
    }

    location /chm {
        proxy_http_version 1.1;
        proxy_set_header Connection "";
        proxy_set_header X-Request-Start "t=${msec}";
        keepalive_timeout 70s;
        proxy_pass http://127.0.0.1:7897;
    }

    location /pms {
        proxy_http_version 1.1;
        proxy_set_header Connection "";
        proxy_set_header X-Request-Start "t=${msec}";
        keepalive_timeout 70s;
        proxy_pass http://127.0.0.1:7897;
    }
}


