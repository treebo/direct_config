server {
        listen   80;
        server_name  assets.treebohotels.com;

        access_log  /var/log/nginx/assets.treebohotels.access.log;

        location / {
                deny all;
        }

        location /static {
		        alias   /opt/treebobackend/webapp/static/;
                allow all;
                gzip_http_version 1.0;
                gzip_static  on;
                add_header   Last-Modified "";
                add_header   Cache-Control public;
		        add_header   Access-Control-Allow-Origin *;
                expires     max;
        }

        location /dist {
                alias   /opt/treebobackend/webapp/static_pipeline/;
                #rewrite ^/static/.*/(scripts|images|styles|html|fonts)/(.*) /static/$1/$2;
                allow all;
                gzip_http_version 1.0;
                gzip_static  on;
                add_header   Last-Modified "";
                add_header   Cache-Control public;
		        add_header   Access-Control-Allow-Origin *;
                expires     max;
        }

        error_page  404  /404.html;

        # redirect server error pages to the static page /50x.html
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
		root   /var/www/nginx-default;
        }
}