server {
   	listen 80;
        set $tid "$pid-$msec-$remote_addr-$request_length";
   	    server_name treebohotels.com www.treebohotels.com www1.treebohotels.com;
        access_log /var/log/nginx/access_json.log le_json;
        error_log /var/log/nginx/error_json.log;

    resolver 10.20.0.2 valid=1s;

    location ~ ^/(api|rest|fot/api|updateRateAmount|updateHotelAvailability|getHotelRatePlan|updateavailability|account)  {

        if ($request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' 'https://gdc.treebohotels.com' always;
            add_header 'Access-Control-Allow-Headers' 'abUserId,Authorization,Accept,Origin,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range' always;
            add_header 'Access-Control-Allow-Methods' 'GET,POST,OPTIONS,PUT,DELETE,PATCH' always;
            add_header 'Access-Control-Allow_Credentials' 'true' always;
            add_header 'Access-Control-Max-Age' 1728000 always;
            add_header 'Content-Type' 'text/plain charset=UTF-8' always;
            add_header 'Content-Length' 0 always;
            return 204;
        }

        add_header 'Access-Control-Allow-Origin' 'https://gdc.treebohotels.com' always;
        add_header 'Access-Control-Allow-Headers' 'abUserId,Authorization,Accept,Origin,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range' always;
        add_header 'Access-Control-Allow-Methods' 'GET,POST,OPTIONS,PUT,DELETE,PATCH' always;
        add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range' always;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto https;
        proxy_set_header ISMOBILE $ismobile;
        proxy_set_header X-Request-Id $tid;
        proxy_redirect off;
        proxy_connect_timeout       7200;
        proxy_send_timeout          7200;
        proxy_read_timeout          310s;
        send_timeout                7200;
        proxy_pass http://localhost:5700;
    }

   location / {
     	return 301 https://www.treebo.com$request_uri;
   }
}
