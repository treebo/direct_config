server {
   listen 80;
   server_name 1treebohotels.com;
#   if ($http_x_forwarded_proto != "https") {
#       rewrite ^(.*)$ https://www.treebohotels.com$request_uri permanent;
#    }
   	return 301 http://www.treebohotels.com$request_uri;
}

server {
    listen   80;
    server_name www.1treebohotels.com;

   if ($http_x_forwarded_proto != "https") {
#   	rewrite ^(.*)$ https://www.treebohotels.com$request_uri permanent;
	rewrite ^/(.*) https://www.treebohotels.com/$1 permanent;
    }

    # no security problem here, since / is alway passed to upstream
    root /home/ubuntu/treebo/treebo/webapp/conf;
    # serve directly - analogous for static/staticfiles
    large_client_header_buffers 4 16k;

    location /alpha {
	return 301 http://alpha.treebohotels.com;
    }

    location /offers {
     	return 301 http://offers.treebohotels.com;
    }

    location /summer {
     	return 301 http://summer.treebohotels.com;
    }

    location /media/ {
        # if asset versioning is used
        if ($query_string) {
            expires 1w;
        }
    }

   location /googlea20d476bb0a5a40c.html {
	root /tmp;
   }
   location /google609fe988d41a43cc.html {
	root /tmp;
   }

   location /google9fe1c66cebe3dc51.html {
	root /tmp;
   }
#location /dist/ {
#                #root /home/ubuntu/treebo/treebo/webapp/static/;
#                alias   /ebs1/treebo-hoist/webapp/dist/;
#                #rewrite ^/static/.*/(scripts|images|styles|html|fonts)/(.*) /static/$1/$2;
#                allow all;
#                gzip_http_version 1.0;
#                gzip_static  on;
#                add_header   Last-Modified "";
#                add_header   Cache-Control public;
#                if ($request_filename ~* ^.*?/([^/]*?)$)
#                {
#                        set $fname $1;
#                }
#
#                if ($fname ~* ^.*?\.(eot)|(ttf)|(ttc)|(otf)|(woff|woff2)$){
#                        add_header   Access-Control-Allow-Origin *;
#                }
#                expires     max;
#        }

   location /static/ {
#    limit_req zone=treebo_ten nodelay;
       autoindex off;
       alias   /ebs1/treebo-web/webapp/static/;

		allow all;
                gzip_http_version 1.0;
                gzip_static  on;
                add_header   Last-Modified "";
                add_header   Cache-Control public;
                if ($request_filename ~* ^.*?/([^/]*?)$)
                {
                        set $fname $1;
                }

                if ($fname ~* ^.*?\.(eot)|(ttf)|(ttc)|(otf)|(woff)|(woff2)$){
                        add_header   Access-Control-Allow-Origin *;
                }
                expires     max;


    }

   location /blog {
#    limit_req zone=treebo_ten nodelay;
	set $request_url "";
	if ($request_uri ~ ^/blog/(.*)$) {
		set $request_url $1;
	}
	return 302 http://blog.treebohotels.com/$request_url;
   }

   location /admin {
       proxy_pass http://www.treebohotels.com/404;
   }

   location /static/admin/ {
        # this changes depending on your python version
        root /home/ubuntu/treebo/lib/python2.7/site-packages/django/contrib/admin;
    }


    location /admin/media/ {
        # this changes depending on your python version
        root /home/ubuntu/treebo/lib/python2.7/dist-packages/django/contrib;
    }

set $mobile_redirect do_not_perform;

if ($http_user_agent ~* "(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino") {
  set $mobile_redirect perform;
}

if ($http_user_agent ~* "^(1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-)") {
  set $mobile_redirect perform;
}

  if ($args ~ 'mobile=true') {
    set $mobile_redirect perform;
    set $mobile_cookie  "mobile=true";
  }

  if ($args ~ 'mobile=false') {
    set $mobile_cookie  "mobile=false";
  }




  add_header Set-Cookie $mobile_cookie;

  if ($http_cookie ~ 'mobile=true') {
    set $mobile_redirect perform;
  }

set $ismobile 0;
if ( $mobile_redirect = perform){
    set $ismobile 1;
}

location  / {
#    limit_req zone=treebo_ten nodelay;

   #if ($http_x__forwarded_proto != "https") {
  # 	 return 301 https://www.treebohotels.com$request_uri;
   # }
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_set_header ISMOBILE $ismobile;
    #add_header X-Request-Id $uuid;
    proxy_redirect off;
    proxy_connect_timeout       7200;
    proxy_send_timeout          7200;
    proxy_read_timeout          310s;
    send_timeout                7200;
    if (!-f $request_filename) {
            set $optproxy A;
    }
    if ( $mobile_redirect = perform) {
            set $optproxy "${optproxy}B";
    }
    if ( $optproxy = AB ){
        proxy_pass http://localhost:5700;
        break;
    }
    if ( $optproxy = A ){
            proxy_pass http://localhost:5700;
            break;
    }
}

location  /reserveRoom/ {
#    limit_req zone=treebo_ten nodelay;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_redirect off;
    proxy_connect_timeout       7200;
    proxy_send_timeout          7200;
    proxy_read_timeout          310s;
    send_timeout                7200;
    if (!-f $request_filename) {
            proxy_pass http://localhost:8082;
            break;
    }
}

location  /qa/ {
#    limit_req zone=treebo_ten nodelay;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_redirect off;
    proxy_connect_timeout       7200;
    proxy_send_timeout          7200;
    proxy_read_timeout          310s;
    send_timeout                7200;
    if (!-f $request_filename) {
            proxy_pass http://localhost:8083;
            break;
    }
}

#location  /login {
##    limit_req zone=treebo_ten nodelay;
#    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
#    proxy_set_header Host $http_host;
#    proxy_redirect off;
#    proxy_connect_timeout       7200;
#    proxy_send_timeout          7200;
#    proxy_read_timeout          310s;
#    send_timeout                7200;
#    if (!-f $request_filename) {
#            proxy_pass http://localhost:8000;
#            break;
#    }
#}
#location  /fot {
#    limit_req zone=treebo_ten nodelay;
#    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
#    proxy_set_header Host $http_host;
#    proxy_redirect off;
#    proxy_connect_timeout       7200;
#    proxy_send_timeout          7200;
#    proxy_read_timeout          310s;
#    send_timeout                7200;
#    if (!-f $request_filename) {
#            proxy_pass http://localhost:8000;
#            break;
#    }
#}
location  /reservations {
#    limit_req zone=treebo_ten nodelay;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_redirect off;
    proxy_connect_timeout       7200;
    proxy_send_timeout          7200;
    proxy_read_timeout          310s;
    send_timeout                7200;
    if (!-f $request_filename) {
            proxy_pass http://localhost:8000;
            break;
    }
}

location  /checkouts {
#    limit_req zone=treebo_ten nodelay;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_redirect off;
    proxy_connect_timeout       7200;
    proxy_send_timeout          7200;
    proxy_read_timeout          310s;
    send_timeout                7200;
    if (!-f $request_filename) {
            proxy_pass http://localhost:8000;
            break;
    }
}


location /sitemap.xml {
     alias /ebs1/treebo-hoist/sitemap.xml;
}

location /robots.txt {
      alias /ebs1/treebo-hoist/robots.txt;
}

}