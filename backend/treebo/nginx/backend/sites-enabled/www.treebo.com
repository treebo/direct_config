server {
    listen 80;
    set $tid "$pid-$msec-$remote_addr-$request_length";
    server_name www.treebo.com www1.treebo.com 10.20.1.195;
    access_log /var/log/nginx/access_json.log le_json;
    error_log /var/log/nginx/error_json.log;

    resolver 10.20.0.2 valid=1s;

    location / {

        set $cors '';
                if ($http_origin ~* 'http?://(tools\.treebohotels\.com)') {
            set $cors 'true';
        }
        if ($cors = 'true') {
            add_header 'Access-Control-Allow-Origin' "$http_origin";
            add_header 'Access-Control-Allow-Credentials' 'true';
            add_header 'Access-Control-Allow-Methods' 'GET';
            add_header 'Access-Control-Allow-Headers' 'Accept,Authorization,Cache-Control,Content-Type,DNT,If-Modified-Since,Keep-Alive,Origin,User-Agent,X-Mx-ReqToken,X-Requested-With';
        }

        if ($request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' 'https://gdc.treebohotels.com' always;
            add_header 'Access-Control-Allow-Headers' 'abUserId,Authorization,Accept,Origin,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range' always;
            add_header 'Access-Control-Allow-Methods' 'GET,POST,OPTIONS,PUT,DELETE,PATCH' always;
            add_header 'Access-Control-Allow_Credentials' 'true' always;
            add_header 'Access-Control-Max-Age' 1728000 always;
            add_header 'Content-Type' 'text/plain charset=UTF-8' always;
            add_header 'Content-Length' 0 always;
            return 204;
        }

        add_header 'Access-Control-Allow-Origin' 'https://gdc.treebohotels.com' always;
        add_header 'Access-Control-Allow-Headers' 'abUserId,Authorization,Accept,Origin,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range' always;
        add_header 'Access-Control-Allow-Methods' 'GET,POST,OPTIONS,PUT,DELETE,PATCH' always;
        add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range' always;

        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        #add_header X-Request-Id $uuid;
        proxy_redirect off;
        proxy_pass http://localhost:5700;
    }

    location ~ /p/ {
        rewrite ^/p/(.*)$ $scheme://bit.ly/$1 last;
    }

    location /alpha {
        return 301 http://alpha.treebohotels.com;
    }

    location /offers {
        return 301 http://offers.treebohotels.com;
    }

    location /summer {
        return 301 http://summer.treebohotels.com;
    }

    location  /blog {
        set $request_url "";
        if ($request_uri ~ ^/blog/(.*)$) {
            set $request_url $1;
        }
        proxy_set_header Host $host;
        proxy_pass http://10.20.1.197:8081/blog/$request_url;
    }

    location /admin {
        proxy_pass http://localhost:5700/404;
    }

    location /robots.txt {
        alias /ebs1/website/current/robots.txt;
    }
}