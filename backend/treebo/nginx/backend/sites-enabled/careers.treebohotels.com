server {
        listen 80;
        server_name careers.treebohotels.com;
        location / {
           return 301 https://careers.treebo.com$request_uri;
        }
}