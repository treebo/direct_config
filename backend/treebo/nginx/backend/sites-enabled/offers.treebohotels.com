server {
    listen          80;
    server_name     offers.treebohotels.com;

    location / {
        proxy_pass http://treebo-offers-35o5.squarespace.com/;
        proxy_redirect http://offers.treebohotels.com http://treebo-offers-35o5.squarespace.com/;
    }

}