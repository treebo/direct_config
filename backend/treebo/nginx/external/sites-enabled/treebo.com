server {
   listen 80;
   server_name treebo.com www.treebo.com;
   return 301 https://www.treebohotels.com$request_uri;
}
