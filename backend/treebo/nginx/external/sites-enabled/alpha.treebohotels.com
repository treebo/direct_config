server {
    listen          80;
    server_name     alpha.treebohotels.com;
#    return 301 http://treebo-alpha-6i91.squarespace.com/;

    location / {
        proxy_pass http://treebo-alpha-6i91.squarespace.com/;
        proxy_redirect http://alpha.treebohotels.com http://treebo-alpha-6i91.squarespace.com/;
    }

    location /alpha {
        index index.html;
        root  /var/www/;
    }
}
