# -*- coding:utf-8 -*-
"""
    Logging Config
"""
import logging
from apps.common.json_encoder import LogstashJsonEncoder


def get(log_root=None, formatter=None, handler=None):
    """
    :param log_root:
    :param formatter:
    :param handler:
    :return:
    """
    if not formatter:
        formatter = 'logstash'

    if not handler:
        handler = 'logging.handlers.WatchedFileHandler'

    if not log_root.endswith('/'):
        log_root += '/'

    return {
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {
            'job_filter': {
                '()': 'base.filters.JobIDFilter'
            },
            'request_filter': {
                '()': 'log_request_id.filters.RequestIDFilter'
            },
            'error_filter': {
                '()': 'base.filters.LogLevelFilter',
                'level': logging.ERROR
            },
            'warn_filter': {
                '()': 'base.filters.LogLevelFilter',
                'level': logging.WARN
            },
            'debug_filter': {
                '()': 'base.filters.LogLevelFilter',
                'level': logging.DEBUG
            },
            'info_filter': {
                '()': 'base.filters.LogLevelFilter',
                'level': logging.INFO
            },
            'require_debug_true': {
                '()': 'django.utils.log.RequireDebugTrue',
            },
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse',
            }
        },
        'formatters': {
            'job_verbose': {
                'format': "[%(job_id)s] [%(request_id)s] [%(asctime)s] %(levelname)s  [%(name)s:%(lineno)s] %(message)s",
                'datefmt': "%d/%b/%Y %H:%M:%S"
            },
            'verbose': {
                'format': "[%(asctime)s] %(levelname)s  [%(name)s:%(lineno)s] %(message)s",
                'datefmt': "%d/%b/%Y %H:%M:%S"
            },
            'simple': {
                'format': '%(levelname)s %(message)s'
            },
            'logstash_fmtr': {
                '()': 'logstash_formatter.LogstashFormatterV1',
                'json_cls': LogstashJsonEncoder
            },
        },
        'handlers': {
            'null': {
                'level': 'INFO',
                'class': 'logging.NullHandler',
            },
            'console': {
                'level': 'INFO',
                'filters': ['require_debug_true'],
                'class': 'logging.StreamHandler',
                'formatter': 'logstash_fmtr'
            },
            'hms': {
                'level': 'ERROR',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/hms.log',
                'formatter': 'logstash_fmtr',
            },
            'celery': {
                'level': 'DEBUG',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/celery.log',
                'formatter': 'logstash_fmtr',
            },
            'error_handler': {
                'level': 'ERROR',
                'filters': ['request_filter'],
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/app_error.log',
                'formatter': 'logstash_fmtr',
            },
            'warn_handler': {
                'level': 'ERROR',
                'filters': ['warn_filter', 'request_filter'],
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/app_warn.log',
                'formatter': 'logstash_fmtr',
            },
            'info_handler': {
                'level': 'INFO',
                'filters': ['request_filter'],
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/app_info.log',
                'formatter': 'logstash_fmtr',
            },
            'debug_handler': {
                'level': 'DEBUG',
                'filters': ['debug_filter', 'request_filter'],
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/app_debug.log',
                'formatter': 'logstash_fmtr',
            },
            'metrics': {
                'level': 'DEBUG',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/metrics.log',
                'formatter': 'logstash_fmtr',
            },
            'django': {
                'level': 'DEBUG',
                'class': 'logging.FileHandler',
                'filename': log_root + '/django.log',
                'filters': ['request_filter'],
                'formatter': 'logstash_fmtr'
            },
            'default': {
                'level': 'DEBUG',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/default.log',
                'formatter': 'logstash_fmtr',
            },
            'hotelogix': {
                'level': 'DEBUG',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/hx.log',
                'formatter': 'logstash_fmtr',
                'filters': ['request_filter'],
            },
            'pricing_api_errors_handler': {
                'level': 'DEBUG',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/pricing_api_errors.log',
                'formatter': 'logstash_fmtr',
                'filters': ['request_filter'],
            },
            'cron': {
                'level': 'DEBUG',
                'class': 'logging.FileHandler',
                'filename': log_root + '/cron.log',
                'formatter': 'logstash_fmtr',
            },
            'mail_admins': {
                'level': 'ERROR',
                'class': 'django.utils.log.AdminEmailHandler',
            },
            'segment': {
                'level': 'DEBUG',
                'class': 'logging.FileHandler',
                'filename': log_root + '/segment.log',
                'formatter': 'logstash_fmtr',
            },
            'booking': {
                'level': 'DEBUG',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/booking.log',
                'formatter': 'logstash_fmtr',
                'filters': ['job_filter'],
            },
            'easyjoblite': {
                'level': 'DEBUG',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/easyjoblite.log',
                'formatter': 'logstash_fmtr',
                'filters': ['job_filter'],
            },
            'hx': {
                'level': 'DEBUG',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/hx.log',
                'formatter': 'logstash_fmtr',
                'filters': ['job_filter'],
            },
            'seo': {
                'level': 'INFO',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/seo.log',
                'formatter': 'logstash_fmtr',
                'filters': ['request_filter'],
            },
            'repositories': {
                'level': 'DEBUG',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/repositories.log',
                'formatter': 'logstash_fmtr',
                'filters': ['request_filter'],},


        },
        'loggers': {
            'django.request': {
                'handlers': ['django'],
                'level': 'DEBUG',
                'propagate': True,
            },
            'celery': {
                'handlers': ['celery'],  # , 'mail_admins'],
                'level': 'DEBUG',
                'propagate': True,
            },
            'hms': {
                'handlers': ['hms'],
                'level': 'INFO',
                'propagate': True,
            },
            'celery.worker': {
                'handlers': ['celery'],  # , 'mail_admins'],
                'level': 'ERROR',
                'propagate': True,
            },
            'cron': {
                'handlers': ['cron'],
                'level': 'DEBUG',
                'propagate': True,
            },
            'hotelogix': {
                'handlers': ['hotelogix'],  # 'mail_admins'],
                'level': 'DEBUG',
                'propagate': True,
            },
            'sendsms': {
                'handlers': ['error_handler', 'warn_handler', 'info_handler', 'debug_handler', 'console'],
                'level': 'DEBUG',
                'propagate': True,
            },
            'metrics': {
                'handlers': ['metrics'],
                'level': 'INFO',
                'propagate': True,
            },
            'base': {
                'handlers': ['error_handler', 'warn_handler', 'info_handler', 'debug_handler', 'console'],
                'level': 'DEBUG',
                'propagate': True,
            },
            'apps': {
                'handlers': ['error_handler', 'warn_handler', 'info_handler', 'debug_handler', 'console'],
                'level': 'DEBUG',
                'propagate': True,
            },
            'pricing_api_errors': {
                'handlers': ['pricing_api_errors_handler', 'error_handler', 'warn_handler', 'info_handler', 'debug_handler',
                             'console'],
                # , 'mail_admins'],
                'level': 'DEBUG',
                'propagate': True,
            },
            'services': {
                'handlers': ['error_handler', 'warn_handler', 'info_handler', 'debug_handler', 'console'],
                'level': 'DEBUG',
            },
            'segment': {
                'handlers': ['segment'],
                'level': 'DEBUG'
            },
            'booking': {
                'handlers': ['booking'],
                'level': 'DEBUG',
                'propagate': True,
            },
            'easyjoblite': {
                'handlers': ['easyjoblite'],
                'level': 'DEBUG',
                'propagate': True,
            },
            'hx': {
                'handlers': ['hx', 'info_handler', 'error_handler'],
                'level': 'INFO',
                'propagate': True,
            },
            'repositories':
                {'handlers':
                     ['repositories', 'info_handler', 'error_handler'],
                 'level': 'DEBUG', 'propagate': True,},
            'seo': {
                'handlers': ['error_handler', 'info_handler'],
                'level': 'INFO',
                'propagate': True,
            },
            'posts': {
                'handlers': ['error_handler', 'info_handler'],
                'level': 'INFO',
                'propagate': True,
            },
            'data_services':
                {'handlers':
                     ['repositories', 'info_handler', 'error_handler'],
                 'level': 'DEBUG', 'propagate': True,},
            '': {
                'handlers': ['default'],  # , 'mail_admins'],
                'level': 'DEBUG',
            },
        },
    }
