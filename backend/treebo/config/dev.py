import logging
import os

from kombu import Exchange, Queue
from kombu.common import Broadcast
from conf import log_conf
from conf.logging_config import LogConfig
from webapp.conf.base import *
from common.exceptions.treebo_exception import TreeboException


def get_host_name():
    hostname = os.environ.get('HOST_NAME')
    return hostname

MIDDLEWARE_CLASSES += ['django.contrib.sessions.middleware.SessionMiddleware']
INSTALLED_APPS += ['django.contrib.sessions', 'shell_plus']
ENVIRONMENT = 'dev'
DOMAIN = 'dev'
FEATURE_TOGGLE_ENV = 'dev'
DEBUG = True
DEVELOPMENT = False

SITE_ID = '6'
TREEBO_BASE_HOST_URL = get_host_name()
TREEBO_WEB_URL = "http://" + TREEBO_BASE_HOST_URL
TREEBO_BASE_URL = '//' + TREEBO_BASE_HOST_URL


APP_VIRALITY_PRIVATE_KEY = 'd97a79edacd64ef8855ba61e00a03a68'
APP_VIRALITY_API_KEY = 'a63403cb67874c838480a61e00a0aba0'
CELERY_HMS_TASK_NAME = 'hms_sync_booking'

CELERY_DEFAULT_QUEUE = 'celery_p3'
CELERY_BROADCAST_BOOKING_QUEUE = 'prod_broadcast_booking'
EXCHANGE = Exchange(CELERY_BROADCAST_BOOKING_QUEUE, type='fanout')

CELERY_CREATE_COUPON_TASK_NAME = "create_coupon_task"
CELERY_CREATE_COUPON_QUEUE = "celery_create_coupon"

CELERY_DYNAMIC_ADS_TASK_NAME='dynamic_ads_task'
CELERY_DYNAMIC_ADS_QUEUE_NAME='dynamic_ads_queue'

CELERY_QUEUES = (
    Queue(
        CELERY_DEFAULT_QUEUE,
        Exchange(CELERY_DEFAULT_QUEUE),
        routing_key=CELERY_DEFAULT_QUEUE),
    Queue(
        CELERY_CREATE_COUPON_QUEUE,
        Exchange(CELERY_CREATE_COUPON_QUEUE),
        routing_key=CELERY_CREATE_COUPON_QUEUE),
    Broadcast(
        CELERY_BROADCAST_BOOKING_QUEUE,
        routing_key='booking.all',
        exchange=EXCHANGE),
    Queue(
        CELERY_THIRD_PARTY_QUEUE,
        THIRD_PARTY_TRIGGER_EXCHANGE,
        routing_key=CELERY_THIRD_PARTY_QUEUE),
    Queue(
        CELERY_TRIVAGO_HOTEL_DETAILS,
        Exchange('trivago'),
        routing_key=CELERY_TRIVAGO_HOTEL_DETAILS),
    Queue(
        CELERY_DYNAMIC_ADS_QUEUE_NAME,
        Exchange(CELERY_DYNAMIC_ADS_QUEUE_NAME),
        routing_key = CELERY_DYNAMIC_ADS_QUEUE_NAME),
)

CELERY_ROUTES = ('base.task_router.MyRouter',)
MAX_WAIT_COUNT_FOR_S2S_CALLBACK = 5

####
# Preprod
CONSUMER_KEY = "587387040D46F439484800A1B35BAB3BEEF99202"
CONSUMER_SECRET = "4040B92975D07B97741AE2DBE76DEB9E1A5B3DC4"
####

DYNAMIC_ADS_FILE_PATH = '/var/data/dynamicads/'

HOTELOGIX_URL = "http://crs.hotelogix.net/ws/web/"
HOTELOGIX_ADMIN_URL = "http://admin.hotelogix.net/ws/web/"

BOOKING_DETAILS_URL = "http://hotelogix.stayezee.com/ws/webv2/"
PAYMENT_SERVICE_HOST = "http://payments-staging.treebo.be/api"

DATABASE_PREPROD = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'web_db',
        'USER': 'web_user',
        'PASSWORD': 'qW4rTwyger685r#',
        'HOST': 'web-s-sharedapps-rds-sg.cadmhukcksta.ap-southeast-1.rds.amazonaws.com',
        'PORT': '5432',
    }
}

DATABASE_DEV = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'treebo',
        'PASSWORD': '',
        'HOST': 'compose_database_1',
        'PORT': '5432',
    }
}

DATABASES = DATABASE_PREPROD

BASE_PRICING_HOST = 'http://price-staging.treebo.be'
BASE_PRICING_URL_V3 = BASE_PRICING_HOST + "/v3/price?"
BASE_PRICING_URL_V2 = BASE_PRICING_HOST + "/v2/price?"
BASE_PRICING_URL_V1 = BASE_PRICING_HOST + "/v1/price?"

PRICING_URL = "https://price-staging.treebo.be/api/v2/hotels/"
STATIC_URL = '//web-staging.treebo.be/dist/'
S3_URL = '//images.treebohotels.com/'
TREEBO_BASE_REST_HOST = TREEBO_WEB_URL

PROWL_HOTEL_UPLOAD = 'http://staging.treebohotels.com/prowl/rest/v1/hotels/add'
PROWL_HOTEL_DISABLE = 'http://staging.treebohotels.com/prowl/rest/v1/hotels/disable'
PROWL_FOT_STATUS_UPDATE_URL = 'http://staging.treebohotels.com/prowl/rest/v1/fotfeedback'
HMS_GET_ALL_RESERVATIONS_FOR_TODAY = 'http://api.treebohotels.com/hmssync/rest/v1/todaysreservations'

AVAILABILITY_HOOK_API = {
    'b2b_axis_room_hook_api': "http://corporates.preprod.treebo.com/ext/api/daywiseInventory/"
}

# razorpay key for test level
RAZORPAY_KEYS = "rzp_test_upYNslfmrDV2tg"
RAZORPAY_SECRET = 'CAzoPh58gaTYCED3RHCJe4Pm'

RAZORPAY_API = "https://api.razorpay.com/v1/payments/"

LOG_REQUESTS = True

FACEBOOK_APP_ID = '920141571373108'
FACEBOOK_APP_SECRET = '395c13ef589da8cc82871e0478c46c42'

# Exclude list from tracking server responses - middleware : response.py
SERVER_STATUS_CAPTURE_FILTER = [200, 301, 302]

EMAIL_USE_TLS = True
EMAIL_HOST = 'email-smtp.eu-west-1.amazonaws.com'
EMAIL_HOST_USER = 'AKIAYZ7GE5SNPHU6ZJWY'
EMAIL_HOST_PASSWORD = 'BNLkNWuJPOSn2WIDtBdiNhl1jLb2LB6kNU7iqkv++7CZ'
EMAIL_PORT = 587
SERVER_EMAIL = 'Treebo Developers <dev@treebohotels.com>'

NOTIFY_HMS_EMAIL = False

AWS_STORAGE_BUCKET_NAME = 'treebo'

CONTACTUS_AVAILABILITY_SUBJECT = 'Hotel room availability query'
CONTACTUS_CORPORATE_SUBJECT = 'Corporate travel agent query'
CONTACTUS_BOOKING_SUBJECT = 'Booking %(category)s query'
CONTACTUS_REFUND_SUBJECT = 'Refund related query'
CONTACTUS_CHECKINOUT_SUBJECT = 'Checkin checkout query'
CONTACTUS_OTHER_SUBJECT = 'Other query'

CONTACTUS_AVAILABILITY_EMAIL_LIST = ['hello@treebohotels.com']
CONTACTUS_CORPORATE_EMAIL_LIST = ['corporate@treebohotels.com']
CONTACTUS_BOOKING_EMAIL_LIST = ['hello@treebohotels.com']
CONTACTUS_REFUND_EMAIL_LIST = ['hello@treebohotels.com']
CONTACTUS_CHECKINOUT_EMAIL_LIST = ['hello@treebohotels.com']
CONTACTUS_OTHER_EMAIL_LIST = ['hello@treebohotels.com']

JOINUS_SUBJECT = 'Join us enquiry'
JOINUS_EMAIL_LIST = ['partners@treebohotels.com']
AVAILABILITY_FROM_BASIC_SEARCH = False
AVAILABILITY_DAY_LIMIT = 30

SHELL_PLUS_PRINT_SQL = True
BROKER_URL = 'amqp://treebo:treebo@' + get_host_name() +':5672/'
CELERY_BROKER_URL = BROKER_URL

PRICING_BROKER_URL = BROKER_URL

GUEST_PREF_SUBJECT = 'Special Request Guest: %(guestname)s, Booking Id: %(bookingid)s, Hotel %(hotelname)s'
GUEST_PREF_EMAIL_LIST = ['direct.tech@treebohotels.com']
HOTELS_FEED_MAIL_LIST = ['direct.tech@treebohotels.com']
BOOKING_REPORT_MAIL_LIST = ['direct.tech@treebohotels.com']

DIRECT_TEAM = ['direct.tech@treebohotels.com']

CANCELLATION_FAILURE_EMAIL_LIST = [] + DIRECT_TEAM

RESERVATION_FAILURE_EMAIL_LIST = [] + DIRECT_TEAM

RACK_RATE_FAILURE = [] + DIRECT_TEAM

ADMINS = [
    ('Direct Tech',
    'direct.tech@treebohotels.com')
]

LOG_PATH_VARIABLE = 'LOG_PATH'
LOG_ROOT = os.getenv(LOG_PATH_VARIABLE, '/var/log/website/')
LOGGING = LogConfig(LOG_ROOT).get()


# Make celery to use the Django root logger config, instead of using its own
CELERYD_HIJACK_ROOT_LOGGER = True
from celery.signals import setup_logging
@setup_logging.connect
def configure_celery_log(sender=None, **kwargs):
    import logging.config
    logging.config.dictConfig(LOGGING)


HOTEL_UPLOAD_FILE_LOCATION = LOG_ROOT
SITE_HOST_NAME = TREEBO_WEB_URL + '/'

REDIS_URL = get_host_name()
REDIS_PORT = 6379
REDIS_DB = 0
CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': REDIS_URL + ':' + str(REDIS_PORT),
        'OPTIONS': {
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'SOCKET_CONNECT_TIMEOUT': 5,
            'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
            'CONNECTION_POOL_CLASS_KWARGS': {
                'max_connections': 50,
                'timeout': 20,
            },
            'PICKLE_VERSION': 2,
        }
    }
}

IMAGE_UPLOAD_PATH = '/ebs1/logs/image_upload/'
POS_URL = 'http://' + get_host_name() + '/api/v1/external/hotelads/updateposfile.xml'
# Third party configs
TRIVAGO_CSV_LINK = '/ebs1/data/trivago/trivago.csv'
TRIVAGO_BID_FILE_PATH = '/ebs1/data/trivago/'

TRANSACTION_URL = "http://" + get_host_name() + "/api/v1/external/hotelads/transactionmessage.xml"
GHA_CSV_EMAIL_ID = ['websales@treebohotels.com']

# GHA Updated Files
FEATURE_POS_MAIL = True
GHA_REPORT_INTERVAL_DAYS = 14

TOOLS_TRANSACTION_URL = "http://tools.treebohotels.com/api/v1/external/hotelads/transactionmessage.xml"
TOOLS_POS_URL = 'http://tools.treebohotels.com/api/v1/external/hotelads/updateposfile.xml'
POS_EMAIL = ["websales@treebohotels.com"]
SAVE_EMAIL_API_FAILURE_EMAIL_ID = ["web@treebohotels.com"]

# trip advisor
TA_CSV_EMAIL_ID = ["direct.tech@treebohotels.com"]
# criteo
CRITEO_CSV_EMAIL_ID = ["direct.tech@treebohotels.com"]

# growth url
GROWTH_ADD_HOTEL_URL = "http://staging.treebohotels.com/growth/common_service/update_hotel_info/"
# GHA
FETCH_BOOKING_STATUS = "http://staging.treebohotels.com/growth/transformer/booking/status/?wrid="

# partial payment growth api url
GROWTH_PARTPAY_URL = 'http://staging.treebohotels.com/growth/partpay/web/link/'

GROWTH_REFERRAL_COMM_API_URL = "http://staging.treebohotels.com/growth/referral/communication"

AUTH_SERVER_HOST = 'http://auth-staging.treebo.be'

APP_CRENDENTIALS = {
    'password': {
        'client_id': 'S5zQjG2Ljd1p3hBL56WKtMrCVnXzUe1nEVEXnlXL',
        'client_secret':
            'hG13WIBlkhgv9WTWGiwQMLcjgSHSt6TV16JJk67w9xpHMQomepvq5gLDSGzZPCvw13Ph4LYuJRhfH8U6UGXS5lzcrmQiu5pYOXm9xjVh0GRxFEoFWFF3bV8Xd5EOLeJA'
    }
}
# by default we use staging
HX_CONFIG = HX_STAGING

BOOKING_CLIENT_TYPE = "HotelogixClient"

EASY_JOB_LITE_SETTINGS_PATH = os.path.join(
    EASY_JOB_LITE_SETTINGS_DIR, ENVIRONMENT + ".yaml")

# cancel hash urls
GROWTH_FETCH_BOOKING_DATA_URL = "http://staging.treebohotels.com/growth/cancellation/cancelbookingdetails/"
GROWTH_SUBMIT_BOOKING_URL = "http://staging.treebohotels.com/growth/cancellation/submitcancellationrequest/"
GROWTH_CANCEL_HASH_URL = "http://staging.treebohotels.com/growth/cancellation/cancelhash"
PROWL_HOTEL_POSITIVES_DATA_URL = "http://staging.treebohotels.com/prowl/rest/v3/hotels/positivereviews/?hotelogix_code={0}&days={1}"
GROWTH_BOOKING_EXP_URL = "http://staging.treebohotels.com/growth/booking_exp/booking_exp_url/"

# seo phase 2
GROWTH_FREQUENTLY_BOOKED_URL = 'http://staging.treebohotels.com/growth/transformer/frequently_booked/'
TRIPADVISOR_BASE_URL = "https://rcp-demo.ext.tripadvisor.com/"
AB_SETTINGS = {
    'is_ab_activated': True,
    'ab_base_url': 'http://abplatform-staging.treebo.be',
    'application_id': 'website',
    'cookies_enabled': False
}

REWARDS_SOURCE_NAME = 'loyalty-service'

CS_QUEUE_NAME = 'cs_queue_dev_'

HOTEL_POLICIES_EMAILS = ['direct.tech@treebohotels.com']

GDC_TEAM_EMAIL = ['direct.tech@treebohotels.com']

HEALTH_CHECK_CONF = dict(rmq_host=BROKER_URL, sqs_queue_name='queue_name', region_name='eu-west-1',
                         aws_secret_access_key='mykey', aws_access_key_id='access_id',
                         soft_dependencies=['CACHEHealthCheck', 'RMQHealthCheck']
                         )

CATALOGUE_SERVICE_RABBITMQ_SETTINGS = {
    'name': 'catalogue_service',
    'url': 'amqp://rms:test@shared-s-rmq-haproxy-node01a.treebo.be:5672/catalog',
    'exchange': 'cs_exchange',
    'routing_key': 'com.cs.property',
}

PDF_URL = "http:{0}{1}?order_id={2}&pretax_price={3}&total_tax={4}"

CATALOGUING_SERVICE_BASE_URL = "http://catalog.treebo.be/cataloging-service/"

BASE_ITS_URL = "http://its-staging.treebo.be"
BASE_TAX_URL_V3 = 'http://tax-staging.treebo.be/tax/v3/calculate_tax'
EMAIL_SERVICE_API = "http://notification-staging.treebo.be/v1/notification/email/"

USE_CRS = True

GROWTH_BROKER_URL = 'amqp://rms:test@shared-s-rmq-haproxy-node01a.treebo.be:5672/rms'
DOMAIN_EVENT_ROUTING_KEY_PREFIX = 'domain_rk_staging_'
GROWTH_EXCHANGE_NAME = 'growth_staging_direct_internal_exchange'
GROWTH_EVENT_TYPE_FOR_BOOKING_STATUS = 'FlaggedReasonEvent'

TAXATION_URL = "http://tax-staging.treebo.be/tax/v2/calculate_tax?breakup=1"

GOOGLE_RECAPTCHA_V3_SECRET_KEY = "6Lc_hIkUAAAAAIzEnqfKgDl1ZieK0dCOc28FUJwi"

DISCOUNT_URL = 'http://koopan.treebo.be/v1/discounts/featured'
COUPON_PRICING_URL = 'http://koopan.treebo.be/v1/discounts/{}/mark_as_applied'
