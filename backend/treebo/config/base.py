# -*- coding: utf-8 -*-
"""
Django settings for webapp project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Guidelines for config files
# ------------------------------------------------------------------------------

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)

import os
from kombu import Exchange, Queue

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

#celery
# ------------------------------------------------------------------------------
CELERY_RESULT_BACKEND = 'djcelery.backends.database:DatabaseBackend'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TRIVAGO_HOTEL_DETAILS = 'trivago_hotel_details_p3'
CELERY_DEFAULT_QUEUE = 'celery_p3'
TASK_IGNORE_RESULT = True
TASK_CREATE_MISSING_QUEUES = False

CELERY_DYNAMIC_ADS_TASK_NAME = 'dynamic_ads_task'
CELERY_DYNAMIC_ADS_QUEUE_NAME = 'dynamic_ads_queue'


CELERY_THIRD_PARTY_QUEUE = 'third_party_trigger_p3'
CELERY_BROADCAST_BOOKING_QUEUE = 'prod_broadcast_booking_p3'
THIRD_PARTY_TRIGGER_EXCHANGE = Exchange('third_party_trigger_p3')
EXCHANGE = Exchange('prod_broadcast_booking_p3', type='fanout')
TRIVAGO_EXCHANGE_NAME = 'trivago_hotel_details_p3'

CELERY_CREATE_COUPON_TASK_NAME = "create_coupon_task"
CELERY_CREATE_COUPON_QUEUE = "celery_create_coupon_p3"

EMAIL_GOOGLE_MAP_IMAGE_SIZE = "500x350"
MODE = 'mob'
MAX_WAIT_COUNT_FOR_S2S_CALLBACK = 5

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'v&$s@g3a7cafqcut=ua(9_%(6janl5q28eul#@*oj!0+)+7j0q'

# DEBUG
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
SEGMENT_DEBUG = True

ALLOWED_HOSTS = ['*']

# MANAGER CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = [('Web Team', 'web@treebohotels.com')]

ENVIRONMENT = 'local'
DOMAIN = 'local'
FEATURE_TOGGLE_ENV = 'local'
SLACK_ALERT_REQUEST_TIMEOUT = 0.500

# Installed apps

APP_VIRALITY_PRIVATE_KEY = 'd97a79edacd64ef8855ba61e00a03a68'
APP_VIRALITY_API_KEY = 'a63403cb67874c838480a61e00a0aba0'

# APP CONFIGURATION
# ------------------------------------------------------------------------------
DJANGO_APPS = [
    "grappelli",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    #"django.contrib.messages",
    "django.contrib.sites",
    "django.contrib.staticfiles",
    "django.contrib.postgres",
    'djcelery',
    'django_extensions',
    'django.contrib.sitemaps',
    'robots'
]

THIRD_PARTY_APPS = [
    "health_check",  # required
    "health_check.db",  # stock Django health checkers
    "health_check.contrib.rmq",
    "health_check.cache",
    "rest_framework",
    "django_mobile",
    "pipeline",
    "django_crontab",
    "django_jenkins",
    "formtools",
    "ab_client.django_extn.ab_config.ABConfig"
]

# Apps specific for treebo go here.
LOCAL_APPS = [
              "apps.ab",
              "apps.pages",
              "apps.hotels",
              "apps.profiles",
              "apps.hms",
              "apps.auth",
              "apps.payments",
              "apps.reward",
              "apps.search",
              "apps.discounts",
              "apps.bookings",
              "apps.pricing",
              "apps.content",
              "apps.featuregate",
              "apps.bookingstash",
              "apps.referral",
              "apps.checkout",
              "base",
              "dbcommon",
              "common",
              "services",
              "scripts",
              "apps.fot",
              "apps.third_party",
              "apps.reviews",
              "apps.intelligent_pah",
              "apps.seo",
              "apps.posts",
              "apps.promotions"
              ]

# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# MIDDLEWARE CONFIGURATION
# ------------------------------------------------------------------------------
MESSAGE_STORAGE = 'django.contrib.messages.storage.cookie.CookieStorage'
MIDDLEWARE_CLASSES = [
    # 'treebo-influx.middleware.InfluxMiddleWare',
    'log_request_id.middleware.RequestIDMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'base.middlewares.treebo_auth_middleware.TreeboAuthMiddleWare',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # TODO: Set these back when reverting back to django 1.9
    'django_mobile.middleware.MobileDetectionMiddleware',
    'django_mobile.middleware.SetFlavourMiddleware',
]
INTERNAL_IPS = ['127.0.0.1']

# Additional Middleware classes
MIDDLEWARE_CLASSES += [
    "base.middlewares.profiler.ProfileMiddleware",
    'pipeline.middleware.MinifyHTMLMiddleware',
    'base.middlewares.is_mobile.MobileDetectionMiddleware',
    'base.middlewares.request.RequestMiddleWare',
    'base.middlewares.set_request_id.UserIDMiddleware',
    'base.middlewares.javascript.BaseJsObjMiddleware',
    'base.middlewares.response.ResponseMiddleWare',
    'base.middlewares.exception.ExceptionMiddleWare',
]


ROOT_URLCONF = 'webapp.urls'

#Requests
# ------------------------------------------------------------------------------
GENERATE_REQUEST_ID_IF_NOT_IN_HEADER = True
REQUEST_ID_RESPONSE_HEADER = "RESPONSE_HEADER_NAME"
LOG_REQUEST_ID_HEADER = 'HTTP_X_REQUEST_ID'
LOG_REQUESTS = True

RACK_RATE_FAILURE = [
    'rohit.jain@treebohotels.com',
    'varun.achar@treebohotels.com',
    'santosh.agsar@treebohotels.com',
    'arun.midha@treebohotels.com',
    'prashant.kumar@treebohotels.com',
    'ashish.mantri@treebohotels.com']

WSGI_APPLICATION = 'webapp.wsgi.application'

AUTHENTICATION_BACKENDS = [
    'base.backends.treebo_auth_backend.TreeboAuthModelBackend']
# AUTHENTICATION_BACKENDS = ['base.backends.treebo_auth_backend.TreeboAuthModelBackend',
#                            'django.contrib.auth.backends.ModelBackend']

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

# GENERAL CONFIGURATION
# ------------------------------------------------------------------------------
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'UTC'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = False

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/dist/'
# S3_URL = 'https://s3-ap-southeast-1.amazonaws.com/treebo/static/'
S3_URL = '//images.treebohotels.com/'
HOTELS_S3_URL = '//images.treebohotels.com/'
TEMPLATE_DEBUG = True
MEDIA_ROOT = os.path.join(BASE_DIR, 'static/files/')
STATIC_ROOT = os.path.join(BASE_DIR, 'static_pipeline')
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'dist'),
)
STATICFILES_STORAGE = 'pipeline.storage.PipelineStorage'

# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#templates
# TEMPLATE_LOADERS = [
#     'django.template.loaders.filesystem.Loader',
#     'django.template.loaders.app_directories.Loader']
TEMPLATES = [
    {
        # See:
        # https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        # See:
        # https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
        'APP_DIRS': True,
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'OPTIONS': {
            # See:
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
            'context_processors': [
                "django.contrib.auth.context_processors.auth",
                'django.template.context_processors.request',
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django_mobile.context_processors.flavour",
                "django.contrib.messages.context_processors.messages",
                "base.context_processors.common_context.extra_context",
            ],
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
            # https://docs.djangoproject.com/en/dev/ref/templates/api/#loader-types
            # 'loaders': [
            #     ('django.template.loaders.cached.Loader', [
            #         # TODO:Uncomment
            #         'django_mobile.loader.Loader',
            #         'django.template.loaders.filesystem.Loader',
            #         'django.template.loaders.app_directories.Loader',
            #     ]),
            # ],
            'builtins':['apps.hotels.templatetags.url_tag']
        }
    }
]

REST_FRAMEWORK = {
    'DATE_INPUT_FORMATS': ['%Y-%m-%d'],

}

LOGIN_URL = '/login/'
# DEFAULT_CHARSET = 'ISO-8859-1'
AUTH_USER_MODEL = 'dbcommon.User'

EMAIL_USE_TLS = True
EMAIL_HOST = 'email-smtp.eu-west-1.amazonaws.com'
EMAIL_HOST_USER = 'AKIAYZ7GE5SNPHU6ZJWY'
EMAIL_HOST_PASSWORD = 'BNLkNWuJPOSn2WIDtBdiNhl1jLb2LB6kNU7iqkv++7CZ'
EMAIL_PORT = 587
SERVER_EMAIL = 'Treebo Developers <dev@treebohotels.com>'
TRIPADVISOR_STAR_RATING_URL = 'https://www.treebo.com/api/v1/reviews/tripadvisor/hotel_review_data/?hotel_id='

# razorpay keys
RAZORPAY_KEYS = "rzp_test_upYNslfmrDV2tg"
RAZORPAY_SECRET = 'CAzoPh58gaTYCED3RHCJe4Pm'

# hms email flag
NOTIFY_HMS_EMAIL = False

REMINDER_EMAIL_SUBJECT = 'Change of plans at %(hotel_name)s? - %(order_id)s'
REMINDER_EMAIL_TIME_CONSTRAINT = 24

AWS_STORAGE_BUCKET_NAME = 'treebo-dev'
AWS_ACCESS_KEY_ID = 'AKIAJSSXURIZXHNCKPNA'
AWS_SECRET_ACCESS_KEY = '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'
S3_STATIC_FILE_PATH = "/static/files/"
S3_STATIC_EXTERNAL_FILE_PATH = "/external-preprod/files/"
EXTERNAL_WEB_URL = "https://external.treebohotels.com"
S3_CITY_FOLDER_PATH = S3_STATIC_FILE_PATH + "/city/"
S3_GENERATED_COUPONS_PATH = "/admin/generated_coupons/"
S3_GENERATED_COUPONS_URL = "https://s3-ap-southeast-1.amazonaws.com/treebo-dev/admin/generated_coupons/"

# AnalyticsCollector constants
# SEGMENT_EVENTS_KEY = 'jRdajGjTNEiBJfgXrsCTwqhp5gXMQdSZ'
SEGMENT_EVENTS_KEYS = {
    'website': 'jRdajGjTNEiBJfgXrsCTwqhp5gXMQdSZ',
    'msite': 'b7knpKDJCZcd14nhK9xw3c2o9hhOhmvM',
    'app': 'ikNqMVL2X5U3ZNOXLUGVUvhGpqcFvlGt'
}

GTM_KEY = 'GTM-M2X4J3'
MAPS_KEY = "AIzaSyBpmnpCC__74pMZiRVequztYER1HAcZ7XM"
GOOGLE_API_SERVER_KEY = 'AIzaSyAULptCOY9nQSaNJ9mYlEFCR-eM-Aj7F98'

ADWORDS_CONVERSION_ID = 0
ADWORDS_CONVERSION_LABEL = ""

SERVER_STATUS_CAPTURE_FILTER = [200, 301]
SERVER_ANALYTICS_EVENT_NAMES = {
    'Completed Order': 'Completed Order',
}

# CRONJOBS = [
#     # ('*/5 * * * *' 'hotels.cron.get_reservations','>> /Users/apple/Documents/treebo-release-6/django-cron.log'),
#     ('*/1 * * * *', 'django.core.management.call_command', ['get_reservations'],'>> ' + BASE_DIR +
#      '/../test-cron.log')
# ]

PIPELINE = ()
PIPELINE_ENABLED = True
PIPELINE_CSS_COMPRESSOR = 'pipeline.compressors.yuglify.YuglifyCompressor'
PIPELINE_JS_COMPRESSOR = 'pipeline.compressors.yuglify.YuglifyCompressor'

STATIC_FILE_VERSION = "2"
PAY_AT_HOTEL = 1
PAY_NOW = 1
CONFIRMATION_MAIL = 1
CANCELLATION_MAIL = 1
OTP = 0
MINIFY_CSS = 0
MINIFY_JS = 0
MODIFY_AMOUNT = 1
AVAILABILITY_FROM_DB = 1
SEND_SIGNUP_COUPON = 0
NOTIFY_MAIL = 1

SMS_VENDOR_ID = "2000145902"
SMS_VENDOR_PASSWORD = "raOkNpg8u"

SMS_PRIORITY_VENDOR_ID = "2000163811"
SMS_PRIORITY_VENDOR_PASSWORD = "musUmR"
SMS_VENDOR_URL = "https://enterprise.smsgupshup.com/GatewayAPI/rest"
TREEBO_BASE_REST_HOST = '//localhost:8000'
TREEBO_BASE_REST_URL = '//localhost:8000/rest/v1'

TREEBO_BASE_HOST_URL = 'www.treebo.com'
TREEBO_WEB_URL = "https://" + TREEBO_BASE_HOST_URL
TREEBO_BASE_URL = '//' + TREEBO_BASE_HOST_URL

FEEDBACK_SENDER = "alerts.webbooking@treebohotels.com"
FEEDBACK_RECEIVER = "alerts.webbooking@treebohotels.com"

GOOGLE_AUTH_URL = 'https://www.googleapis.com/oauth2/v3/tokeninfo'
TRIP_ADVISOR_BASE_URL = 'http://api.tripadvisor.com/api/partner/2.0'
TRIP_ADVISOR_API_KEY = ''

BROKER_URL = 'amqp://'
PRICING_BROKER_URL = 'amqp://'
GRAPPELLI_ADMIN_TITLE = 'Treebo Admin Site'
APPVIRALITY_SHARE_LINK_QUERY_PARAM = 'avclk'
APPVIRALITY_SECURE_KEY = "72d8b1de-432b-11e6-beb8-9e71128cae77"

SITE_ID = '4'
# Redis Configuration
REDIS_URL = 'localhost'
REDIS_PORT = 6379
REDIS_DB = 0
CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': REDIS_URL + ':' + str(REDIS_PORT),
        'OPTIONS': {
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'SOCKET_CONNECT_TIMEOUT': 5,
            'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
            'CONNECTION_POOL_CLASS_KWARGS': {
                'max_connections': 50,
                'timeout': 20,
            },
            'PICKLE_VERSION': 2,
        }
    }
}

CACHE_INVALIDATE_ON_CREATE = 'whole-model'

# Influx DB Config Start
"""
Check https://bitbucket.org/treebo/treebo_commons
to get setup steps.
"""
INFLUX = {
    'default': {
        'NAME': 'website',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': '172.40.10.39',
        'PORT': '8086',
    }
}
# could be for a lesser duration ex 1, 2, 5, etc.
INFLUX_RETENTION_POLICY = "INF"
INFLUX_UDP = False
INFLUX_UDP_PORT = 4444
INFLUX_RETENTION_REPLICATION = 1
# Influx DB Config End

DEFAULT_PAYMENT_GATEWAY = 'RAZORPAY'

RAZORPAY_GATEWAY = 'razorpay'
TREEBO_WALLET_GATEWAY = 'treebo_wallet'
PHONEPE_GATEWAY = 'phonepe'
AMAZONPAY_GATEWAY = 'amazonpay'
PHONEPE_APP_GATEWAY = 'phonepe_app'
AVAILABILE_GATEWAY = [RAZORPAY_GATEWAY, PHONEPE_GATEWAY, AMAZONPAY_GATEWAY, PHONEPE_APP_GATEWAY]

TREEBO_HELPLINE = "+91 9322800100"
HELPLINE_CALLER = "tel::(+91) 9322800100"
DYNAMIC_ADS_FILE_PATH = '/dynamicads/'
RAZORPAY_API = "https://api.razorpay.com/v1/payments/"
FEATURE_POS_MAIL = True

SITE_HOST_NAME = 'http://127.0.0.1/'
SITE_ID = '6'

FIRST_BLOCKOUT_RANGE_START = '2016-12-23'
FIRST_BLOCKOUT_RANGE_END = '2017-01-03'

SECOND_BLOCKOUT_RANGE_START = '2017-03-24'
SECOND_BLOCKOUT_RANGE_END = '2017-03-26'

# Third party configs
TRIVAGO_URL_LIMIT_EXCEED_RECEIVER_EMAILS = ['sumit.chourasia@treebohotels.com',
                                            'vivek.gupta@treebohotels.com']
TRIVAGO_BID_FILE_NAME = 'trivago_bid_file_from_treebo.csv'
EMAIL_SERVICE_API = "http://notification.treebo.pr/v1/notification/email/"
CHILDREN_AGE = 8

TREEBO_SENDER_EMAIL = "alerts.webbooking@treebohotels.com"
TREEBO_SENDER_NAME = "Treebo Hotels"
GENERATED_COUPONS_RECEIVER_EMAIL = [
    'sunil.rana@treebohotels.com',
    'arun.midha@treebohotels.com']

# saveemailapi failure configs
SAVE_EMAIL_API_FAILURE_SUBJECT = "FAILURE OF SAVEEMAILCONFIGURATION API FOR HOTEL %s DURING HOTEL UPLOAD PROCESS"
SAVE_EMAIL_API_FAILURE_CONTENT = "The saveemailconfiguration API of Hotelogix has been failed. " \
                                 "Please check on tools machine (172.40.10.196) in /ebs1/logs/app_error.log " \
                                 "and get it rectified."
# GHA configs
POS_SUBJECT = "UPDATED POINT OF SALE FILE FOR HOTEL %s(%s)"
POS_EMAILBODY = "Please find the attached updated POINT OF SALE File and send this updated file to the google hotel ads"
TRANSACTION_SUBJECT = "UPDATED TRANSACTION FILE FOR HOTEL %s(%s)"
TRANSACTION_EMAILBODY = "Please find the attached updated TRANSACTION RESPONSE AND REQUEST FILE and " \
                        "send these files to the google hotel ads"

WAIT_TIME_TO_FETCH_GUEST_FROM_GROWTH = 2400
GROWTH_API_URL = 'http://growth.treebohotels.com/api/'

REFERRALCODE = 'REFERRAL500'
FB_SHARE_URL = "https://www.facebook.com/share.php?u="
TWITTER_SHARE_URL = "http://twitter.com/home/?status="
GROWTH_REFERRAL_COMM_API_URL = "http://growth.treebohotels.com/growth/referral/communication"
AVAILABLE_COUPONS_LIMIT = 5

# AUTH SERVER CONFIG
AUTH_SERVER_HOST = 'http://172.40.10.74:8051'
AUTH_URL_ENDPOINTS = {
    "token_validate": "/treeboauth/auth/token/validate",
    "login": "/treeboauth/auth/token",
    "authorized_token": "/treeboauth/auth/authorized-token",
    "logout": "/treeboauth/profile/v1/logout",
    "change_password": "/treeboauth/profile/v1/change-password-token",
    "forgot_password": "/treeboauth/profile/v1/forgot-password",
    "reset_password": "/treeboauth/profile/v1/reset-password",
    "email_verification": "/treeboauth/profile/v1/email-verification",
    "register": "/treeboauth/profile/v1/register",
    "login_via_otp": "/treeboauth/profile/v1/otp/login",
    "login_via_otp_verify": "/treeboauth/profile/v1/otp/verify",
    "otp_is_verified": "/treeboauth/profile/v1/otp/is-verified",
    "convert_token": "/treeboauth/auth/convert-token",
    "user_detail": "/treeboauth/profile/v1/user/detail/",
    "update_profile": "/treeboauth/profile/v1/update",
}

TREEBO_WEB_EMAIL_VERIFY_URL = '/api/v2/auth/email-verify/'
TREEBO_WEB_RESET_PASSWORD_URL = '/user/reset-password/'

# TA Configs
REVIEW_CACHE_TIMEOUT = 90000
REVIEW_CACHE_KEY_PREFIX = 'REVIEW'
TA_BASE_URL = "http://api.tripadvisor.com/api/partner/2.0/location/"
TA_PARTNER_KEY = "08BE1C5F05A243288E5D83907AAC6DF8"
TA_URL_PARAMS = "?lang=en_US&all_shared={0}&key=" + TA_PARTNER_KEY + "&limit=100&offset=0"

# slack alert url for reviews
SLACK_CHANNEL_REVIEW_ALERT_URL = 'https://hooks.slack.com/services/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'
GROWTH_PARTPAY_URL = 'http://staging.treebohotels.com/growth/partpay/web/link/'

# cancel hash urls
GROWTH_FETCH_BOOKING_DATA_URL = "http://growth.treebohotels.com/growth/cancellation/cancelbookingdetails/"
GROWTH_SUBMIT_BOOKING_URL = "http://growth.treebohotels.com/growth/cancellation/submitcancellationrequest/"
BOOKING_LOGGER = 'booking'

# Easyjoblite
EASY_JOB_LITE_SETTINGS_DIR = os.path.join(BASE_DIR, 'conf/easyjoblite')
EASY_JOB_LITE_SETTINGS_PATH = os.path.join(
    EASY_JOB_LITE_SETTINGS_DIR, ENVIRONMENT + ".yaml")

# Cache keys
MINPRICE_KEY = 'min_price_across_all_hotels'
MAXPRICE_KEY = 'max_price_across_all_hotels'

#default values for dynamic price in meta tags
DEFAULT_PRICE = 999
DEFAULT_SELL_PRICE = 999
DEFAULT_RACK_RATE = 1299
DEFAULT_DISCOUNT = 23
DEFAULT_MAX_PRICE = 4999

# number of localities/landmarks to display on hd page
HD_LOCATION_CAP = 11

# HX Client Settings
# -----------------------------------------------------


class HxConfig(object):
    """
        HxConfig
    """

    def __init__(self, url, consumer_key, consumer_secret,
                 counter_login, counter_password, payment_title):
        self.url = url

        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret

        self.counter_login = counter_login
        self.counter_password = counter_password

        self.payment_title = payment_title


HX_STAGING = HxConfig(url='http://crs.hotelogix.net/ws/web',
                      consumer_key="587387040D46F439484800A1B35BAB3BEEF99202",
                      consumer_secret='4040B92975D07B97741AE2DBE76DEB9E1A5B3DC4',
                      counter_login='testquality@treebohotels.com',
                      counter_password='T@stteAM1',
                      payment_title='cash')



HX_PROD = HxConfig(url='https://crs.staygrid.com/ws/web',
                   consumer_key="B39CEE50AC4B6925F9697409E2D84C88746593E5",
                   consumer_secret='9110CC27311F94542E7F5ACF3643B3909029F570',
                   counter_login='redutil@treebohotels.com',
                   counter_password='redemptionbot',
                   payment_title='Bill to Company (Treebo Collectible)')

HX_CONFIG = HX_STAGING

GROWTH_CANCEL_HASH_URL = "http://growth.treebohotel.in/growth/cancellation/cancelhash"
INTELLIGENT_PAH_IS_AUDIT_ENABLED = True

# cataloguing apis
CATALOG_HOTEL_URL = 'http://staging.treebohotels.com/cataloging-service/properties'
CATALOG_ROOMS_URL = 'http://staging.treebohotels.com/cataloging-service/properties/{0}/rooms/'
CATALOG_HOTEL_FACILITIES_URL = 'http://staging.treebohotels.com/cataloging-service/properties/{0}/amenities/'
CATALOG_ROOM_FACILITIES_URL = 'http://staging.treebohotels.com/cataloging-service/properties/{0}/rooms/{1}/amenities/'
CATALOG_BANQUET_HALL_URL = 'http://staging.treebohotels.com/cataloging-service/properties/{0}/banquet-halls/'
CATALOG_RESTAURANTS_URL = 'http://staging.treebohotels.com/cataloging-service/properties/{0}/restaurants/'
CATALOG_BARS_URL = 'http://staging.treebohotels.com/cataloging-service/properties/{0}/bars/'


# paymnet service config
# PAYMENT_SERVICE_HOST = 'http://172.40.20.183:8003'
PAYMENT_SERVICE_HOST = 'http://172.40.20.210:8003'
PAYMENT_SERVICE_URL_ENDPOINTS = {
    "order": "/paynow/order/",
    "order2": "/paynow/v2/order/",
    "payment": "/paynow/payment/",
    "payment2": "/paynow/v2/verify/",
    "customer": "/paynow/customer/",
}

NRP_ENABLED = True

USERSERVICE_HOST = 'booking'
USERSERVICE_ENDPOINTS = {
    "userid": "/paynow/order/",
}

# seo phase 2
GROWTH_FREQUENTLY_BOOKED_URL = 'http://growth.treebohotel.in/growth/transformer/frequently_booked/'
TRIPADVISOR_BASE_URL = "https://www.tripadvisor.com/"
JCR_URL = "WidgetEmbed-collectreview?overallRating=5&" \
          "cleanlinessSubrating=5&locationSubrating=5&roomsSubrating=5&serviceSubrating=5" \
          "&valueSubrating=5&sleepqualitySubrating=5&" \
          "partnerId={0}&lang=en_IN&locationId={1}&display=true&allowMobile"
TA_JCR_URL_PARTNER_KEY = "454DF6004C8847DC83E6144B85765433"
TA_JCR_URL_ENCRYPTION_KEY = "8&FRzT6DrF_d%N,a1mM@T0&SGhBnV_mr"
JCR_URL_GUEST_DATA = "locationId={0}&email={1}"
REDIRECT_URL = "&redirectUrl={0}"
REVIEW_REMINDER_IFRAME_API_URL = "/api/v1/reviews/tripadvisor/jcr_url_generation/?hotel_id={0}&email={1}&name={2}"
WEATHER_API_URL = 'https://api.darksky.net/forecast/f1ff8acdab652a7a6d09d0948ddbbdf8/'
WEATHER_API_URL_ALT = 'http://api.openweathermap.org/data/2.5/forecast/daily?units=imperial&type=accurate&mode=json&APPID=92fbf9e755654190e1442c471ed9c10c&'

DEFAULT_SEO_LANDMARK_RADIUS = 2

#AB
AB_SETTINGS = {
    'is_ab_activated': False,
    'ab_base_url': 'http://ab1.treebohotel.in',
    'application_id': 'website',
    'cookies_enabled': False
}

HX_GATEWAY_CONFIG = {
    'treebo_wallet': {
        'payment_mode': 'oth',
        'payment_type': 'TP',
    },
    'razorpay': {
        'payment_mode': 'oth',
        'payment_type': 'WEB',
    },
    'phonepe': {
        'payment_mode': 'oth',
        'payment_type': 'PP',
    },
    'amazonpay': {
        'payment_mode': 'oth',
        'payment_type': 'AP',
    },
    'phonepe_app': {
        'payment_mode': 'oth',
        'payment_type': 'PP',
    }

}

#Loyalty
APPLY_WALLET = True
WALLET_TYPE = 'TP'
WALLET_BURN_EVENT_TYPE = 'room_booking'
REWARDS_SOURCE_NAME = 'web_direct'
REWARDS_SOURCE_TYPE = 'burning_source'
STAGING = 'staging'

NOTIFICATION_API_HOST = 'http://notification.treebo.pr'

HEALTH_CHECK_CONF = dict(rmq_host=BROKER_URL, sqs_queue_name='queue_name', region_name='eu-west-1',
                         aws_secret_access_key='mykey', aws_access_key_id='access_id',
                         use_basic_db_check=True,
                         soft_dependencies=['CACHEHealthCheck', 'RMQHealthCheck']
                         )
# AUTO_APPLY_CONFIG
ENABLE_TRIVAGO_AUTO_APPLY = True
TRIVAGO_UTM_SOURCE = 'Trivago'
TRIVAGO_EXTERNAL_UTM_SOURCE = 'Trivago_external'
TRIPADVISOR_UTM_SOURCE = 'tripadvisor'
GOOGLE_ADS_UTM_SOURCE = 'googlehotelads'

IXIGO_UTM_SOURCE = 'ixigo'
MAX_NUMBER_OF_IMAGES_TO_BE_USED = 20

# PROWL
PROWL_HOTEL_UPLOAD = 'https://dummy/url/to/hotelupload/'
PROWL_HOTEL_DISABLE = 'http://dummy/url/to/hotelupload/'

# Catalogue Service Settings
#-------------------------------------------------------------------------
CATALOGUE_SERVICE_RABBITMQ_SETTINGS = {
    'name': 'catalogue_service',
    'url': 'amqp://guest:guest@172.40.20.210:5672//',
    'exchange': 'cs_exchange',
    'routing_key': 'com.cs.property',
}

CS_QUEUE_NAME = 'catalogue_service_queue'


HOTEL_POLICY_SUBJECT_ON_DETACH = '[Urgent: Action Required - {0}({1}) is no longer accepting couple friendly (and /or)' \
                  ' local id bookings]'

HOTEL_POLICY_SUBJECT_ON_ADD = '[Urgent:  New hotel added - {0}({1}) is now accepting couple friendly (and /or)' \
                              ' local id bookings]'

CATALOGUING_SERVICE_BASE_URL = "http://staging.treebohotels.com/cataloging-service/"

# Frequency at which rabbitMq heartbeats
RABBIT_MQ_HEART_BEAT_INTERVAL = 5

#The policy for rabbitMq for retring to publish
RABBIT_MQ_RETRY_POLICY = {
             'interval_start': 0,  # First retry immediately,
             'interval_step': 2,  # then increase by 2s for every retry.
             'interval_max': 10,  # but don't exceed 10s between retries.
             'max_retries': 3,  # give up after 3 tries.
         }

RABBIT_MQ_TRANSPORT_OPTIONS = {'confirm_publish': True}

RABBIT_MQ_PREFETCH_COUNT = 8

PHONEPE_SERVER = 'https://mercury-uat.phonepe.com'
CONFIRMATION_PAGE_URL = '/confirmation/{}/?rateplan={}&status={}'
PAYMENT_PROCESS_URL = '/payment-process/{}/'
ITINERY_PAGE_URL = '/itinerary/?'

NOTIFICATION_HOST = 'http://api.treebohotels.com'
EMAIL_NOTIFICATION_ENDPOINT = '/v1/notification/email/'
SMS_NOTIFICATION_ENDPOINT = '/v1/notification/sms/'
NOTIFICATION_RETRY_COUNT = 3

PDF_URL = "https:{0}{1}?order_id={2}&pretax_price={3}&total_tax={4}"
AVAILABILITY_CORRECTION = 0
GDC_TEAM_EMAIL=['gdcteam@treebohotels.com','sandheep.kumar@treebohotels.com',
                'peeyush.panthari@treebohotels.com','satish.panicker@treebohotels.com',
                'gdswat@treebohotels.com']

CS_QUEUE_NAME = 'cs_queue_local'

USE_ITS_APIS = True
BASE_ITS_URL = "http://its.treebo.com"

USE_CRS = True
HOLD_TIME_IN_MINS_TO_CONFIRM_BOOKING = 60

CRS_INTEGRATION_EVENT_RABBITMQ_SETTING = {
    'name': 'crs_integration',
    'url': 'amqp://crs:Mb500@shared-s-rmq-haproxy-node01a.treebo.be:5672/crs',
    'exchange': 'integration_event_exchange',
    'routing_key': 'hotel.migrated',
}
CRS_INTEGRATION_EVENT_QUEUE_NAME = 'integration_event_queue_direct_staging'
BOOKING_FAILED_DURING_HOTEL_MIGRATION_ERR_MSG = 'Hotel is not managed by CRS'

BASE_TAX_URL_V3 = 'http://tax.treebo.com/tax/v3/calculate_tax'
TAX_URL = "http://tax-staging.treebo.be/tax/v4/calculate_tax"
CS_ROOM_TYPE_MAPPING = {
    'Acacia': 'rt01',
    'Oak': 'rt02',
    'Maple': 'rt03',
    'Mahogany': 'rt04'
}

#SEARCH API
SEARCH_DEFAULT_SEO_DISTANCE_CAP = 10
SEARCH_DEFAULT_CITY_DISTANCE_CAP = 20

HOMESTAY_NAMESPACE = "homestays"
HOMESTAY_FEATURE_NAME = "external_provider_integration"
DEFAULT_PROVIDER_NAME = "treebo"
DEFAULT_EXTERNAL_ROOM_DISPLAY_NAME = "Hut with Breakfast and Dinner"
DEFAULT_RATE_PLAN = "EP"

BOOKING_CANCEL_URL_HASH_CODE_LENGTH = 6
PARTIAL_PAYMENT_POLICY_CONTENT_KEY_NAME = "partial_payment_policy"

TRIVAGO_TREEBO_ADVERTISER_ID = 1894
TRIVAGO_SUPERHERO_ADVERTISER_ID = 2958
PSYC_TRIGGER_GET_PRIORITY = "https://zoq8b5ygsa.execute-api.ap-southeast-1.amazonaws.com/staging/"

GOOGLE_RECAPTCHA_V3_VERIFY_URL = "https://www.google.com/recaptcha/api/siteverify"
GOOGLE_RECAPTCHA_V3_SECRET_KEY = "6LeahIkUAAAAAEV5aGVtY5GG3rJUvaC9v5VSwfKI"  # staging conf
GOOGLE_RECAPTCHA_V3_THRESHOLD_SCORE = 0.5

DISCOUNT_URL = 'http://koopan.treebo.be/v1/discounts/featured'
COUPON_PRICING_URL = 'http://koopan.treebo.be/v1/discounts/{}/mark_as_applied'
DISCOUNT_HOST = 'http://koopan.treebo.be'