import logging

from kombu import Exchange, Queue
from kombu.common import Broadcast
from conf import log_conf
from .base import *
from conf.logging_config import LogConfig



MIDDLEWARE_CLASSES += ('django.contrib.sessions.middleware.SessionMiddleware',)
INSTALLED_APPS += ['django.contrib.sessions']

ENVIRONMENT = 'prod'
DOMAIN = 'tools'
FEATURE_TOGGLE_ENV = 'prod'
DEBUG = False
DEVELOPMENT = False
TEMPLATE_DEBUG = False
GROWTH_PARTPAY_URL = 'http://growth.treebohotels.com/growth/partpay/web/link/'
SITE_ID = '6'

APP_VIRALITY_PRIVATE_KEY = 'd97a79edacd64ef8855ba61e00a03a68'
APP_VIRALITY_API_KEY = 'a63403cb67874c838480a61e00a0aba0'

BASE_PRICING_HOST = 'http://pricing.treebohotels.com'
BASE_PRICING_URL_V3 = BASE_PRICING_HOST + "/v3/price?"
BASE_PRICING_URL_V2 = BASE_PRICING_HOST + "/v2/price?"
BASE_PRICING_URL_V1 = BASE_PRICING_HOST + "/v1/price?"

# for coupon script
CELERY_CREATE_COUPON_TASK_NAME = "create_coupon_task"
CELERY_CREATE_COUPON_QUEUE = "celery_create_coupon"

CELERY_BROADCAST_BOOKING_QUEUE = 'prod_broadcast_booking'
CELERY_HMS_TASK_NAME = 'hms_sync_booking'
EXCHANGE = Exchange(CELERY_BROADCAST_BOOKING_QUEUE, type='fanout')


CELERY_ROUTES = (
    {
        CELERY_CREATE_COUPON_TASK_NAME: {
            'queue': CELERY_CREATE_COUPON_QUEUE,
            'routing_key': CELERY_CREATE_COUPON_QUEUE
        }
    },
    {
        CELERY_TRIVAGO_HOTEL_DETAILS: {
            'queue': CELERY_TRIVAGO_HOTEL_DETAILS,
            'routing_key': CELERY_TRIVAGO_HOTEL_DETAILS
        }
    },
)

CELERY_QUEUES = (
    Queue(
        CELERY_DEFAULT_QUEUE,
        Exchange(CELERY_DEFAULT_QUEUE),
        routing_key=CELERY_DEFAULT_QUEUE),
    Queue(
        CELERY_CREATE_COUPON_QUEUE,
        Exchange(CELERY_CREATE_COUPON_QUEUE),
        routing_key=CELERY_CREATE_COUPON_QUEUE),
    Queue(
        CELERY_TRIVAGO_HOTEL_DETAILS,
        Exchange('trivago'),
        routing_key=CELERY_TRIVAGO_HOTEL_DETAILS),
    Broadcast(
        CELERY_BROADCAST_BOOKING_QUEUE,
        routing_key='booking.all',
        exchange=EXCHANGE),
)

CONSUMER_KEY = "B39CEE50AC4B6925F9697409E2D84C88746593E5"
CONSUMER_SECRET = "9110CC27311F94542E7F5ACF3643B3909029F570"
DYNAMIC_ADS_FILE_PATH = '/ebs1/dynamicads/'
HOTELOGIX_URL = "https://crs.staygrid.com/ws/web/"
HOTELOGIX_ADMIN_URL = "https://admin.staygrid.com/ws/web/"
BOOKING_DETAILS_URL = "https://crs.staygrid.com/ws/webv2/"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'treebo',
        'USER': 'treeboadmin',
        'PASSWORD': 'caTwimEx3',
        'HOST': 'pgb-web-mum.treebohotels.com',
        'PORT': '6432',
    }
}

PRICING_URL = "https://preprod.treebo.com/api/v2/hotels/"
STATIC_URL = '//tools.treebohotels.com/dist/'
S3_URL = '//images.treebohotels.com/'
TREEBO_BASE_URL = '//tools.treebohotels.com'

# PAYU PROD KEYS
PAYU_MERCHANT_ID = 'xBeWGL'
PAYU_SALT = 'cS9H6XPi'
PAYU_HANDLER_URL = '//preprod.treebo.com/checkoutaction/'
PAYU_ACTION_URL = 'https://secure.payu.in/_payment'
PROWL_HOTEL_UPLOAD = 'http://api.treebohotels.com/prowl/rest/v1/hotels/add'
PROWL_HOTEL_DISABLE = 'http://api.treebohotels.com/prowl/rest/v1/hotels/disable'
PROWL_FOT_STATUS_UPDATE_URL = 'http://api.treebohotels.com/prowl/rest/v1/fotfeedback'
HMS_GET_ALL_RESERVATIONS_FOR_TODAY = 'http://api.treebohotels.com/hmssync/rest/v1/todaysreservations'

RAZORPAY_KEYS = "rzp_live_USOzRto8cqRWYo"
RAZORPAY_SECRET = 'p6CotYbheCsbIVuOL7yL8BI2'
RAZORPAY_API = "https://api.razorpay.com/v1/payments/"
LOG_REQUESTS = True

FACEBOOK_APP_ID = '920141571373108'
FACEBOOK_APP_SECRET = '395c13ef589da8cc82871e0478c46c42'

# Exclude list from tracking server responses - middleware : response.py
SERVER_STATUS_CAPTURE_FILTER = [200, 301, 302]

EMAIL_USE_TLS = True
EMAIL_HOST = 'email-smtp.eu-west-1.amazonaws.com'
EMAIL_HOST_USER = 'AKIAYZ7GE5SNPHU6ZJWY'
EMAIL_HOST_PASSWORD = 'BNLkNWuJPOSn2WIDtBdiNhl1jLb2LB6kNU7iqkv++7CZ'
EMAIL_PORT = 587
SERVER_EMAIL = 'Treebo Hotels <alerts.webbooking@treebohotels.com>'

AWS_STORAGE_BUCKET_NAME = 'treebo'

# SEGMENT_EVENTS_KEY = 'jRdajGjTNEiBJfgXrsCTwqhp5gXMQdSZ'

CONTACTUS_AVAILABILITY_SUBJECT = 'Hotel room availability query'
CONTACTUS_CORPORATE_SUBJECT = 'Corporate travel agent query'
CONTACTUS_BOOKING_SUBJECT = 'Booking %(category)s query'
CONTACTUS_REFUND_SUBJECT = 'Refund related query'
CONTACTUS_CHECKINOUT_SUBJECT = 'Checkin checkout query'
CONTACTUS_OTHER_SUBJECT = 'Other query'

CONTACTUS_AVAILABILITY_EMAIL_LIST = ['alerts.webbooking@treebohotels.com']
CONTACTUS_CORPORATE_EMAIL_LIST = ['corporate@treebohotels.com', 'arunabh.sinha@treebohotels.com',
                                  'niharika.singh@treebohotels.com']
CONTACTUS_BOOKING_EMAIL_LIST = ['alerts.webbooking@treebohotels.com']
CONTACTUS_REFUND_EMAIL_LIST = ['alerts.webbooking@treebohotels.com']
CONTACTUS_CHECKINOUT_EMAIL_LIST = ['alerts.webbooking@treebohotels.com']
CONTACTUS_OTHER_EMAIL_LIST = ['alerts.webbooking@treebohotels.com']

JOINUS_SUBJECT = 'Join us enquiry'
JOINUS_EMAIL_LIST = ['partners@treebohotels.com']
AVAILABILITY_FROM_BASIC_SEARCH = False
AVAILABILITY_DAY_LIMIT = 30

BROKER_URL = 'amqp://web:M3b00@shared-rmq.treebo.com:5672/web'
PRICING_BROKER_URL = BROKER_URL

GUEST_PREF_SUBJECT = 'Special Request Guest: %(guestname)s, Booking Id: %(bookingid)s, Hotel %(hotelname)s'
GUEST_PREF_EMAIL_LIST = ['rohit.jain@treebohotels.com']
HOTELS_FEED_MAIL_LIST = [
    'ranavishal.singh@treebohotels.com',
    'shekhar.gupta@treebohotels.com',
    'rohit.jain@treebohotels.com']
BOOKING_REPORT_MAIL_LIST = ['ranavishal.singh@treebohotels.com',
                            'rohit.jain@treebohotels.com']
CANCELLATION_FAILURE_EMAIL_LIST = [
    'rohit.jain@treebohotels.com',
    'varun.achar@treebohotels.com',
    'prashant.kumar@treebohotels.com',
    'sunil.rana@treebohotels.com',
    'thulasi.ram@treebohotels.com',
    'rohit.jain@treebohotels.com',
    'varun.achar@treebohotels.com',
]
RESERVATION_FAILURE_EMAIL_LIST = ['prashant.kumar@treebohotels.com',
                                  'sunil.rana@treebohotels.com',
                                  'thulasi.ram@treebohotels.com',
                                  'rohit.jain@treebohotels.com',
                                  'varun.achar@treebohotels.com',
                                  'tech-qa@treebohotels.com']

RACK_RATE_FAILURE = [
    'rohit.jain@treebohotels.com',
    'varun.achar@treebohotels.com',
    'santosh.agsar@treebohotels.com',
    'arun.midha@treebohotels.com',
    'prashant.kumar@treebohotels.com',
    'ranavishal.singh@treebohotels.com'
    'ashish.mantri@treebohotels.com']

ADMINS = [
    ('Rohit Jain',
     'rohit.jain@treebohotels.com'),
    ('Varun Achar',
     'varun.achar@treebohotels.com'),
    ('Tech QA',
     'tech-qa@treebohotels.com'),
    ('Prashant Kumar',
     'prashant.kumar@treebohotels.com'),
    ('Veena Ampabathini',
     'veena.ampabathini@treebohotels.com'),
    ('Amitakhya Bhuyan',
     'amitakhya.bhuyan@treebohotels.com'),
    ('Piyush Trivedi',
     'piyush.trivedi@treebohotels.com')
    ]

# OVERWRITE LOGGERFRAMEWORK
LOG_ROOT = os.environ.setdefault('LOG_ROOT', '/var/log/website/')
LOGGING = LogConfig(LOG_ROOT).get()


# Make celery to use the Django root logger config, instead of using its own
CELERYD_HIJACK_ROOT_LOGGER = False

from celery.signals import setup_logging


@setup_logging.connect
def configure_celery_log(sender=None, **kwargs):
    import logging.config
    logging.config.dictConfig(LOGGING)


HOTEL_UPLOAD_FILE_LOCATION = LOG_ROOT

# Redis Configuration
#REDIS_URL = 'cache-p-02.treebo.pr'
#REDIS_PORT = 6379
#REDIS_DB = 0
REDIS_URL = 'cache-p-02.treebo.pr'
#REDIS_URL = 'localhost'
REDIS_PORT = '6379'
REDIS_DB = '4'
CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        #'BACKEND': 'redis_cache.backends.dummy.RedisDummyCache',
        'LOCATION': 'redis://' + REDIS_URL + ':' + str(REDIS_PORT) + '/' + REDIS_DB,
        'OPTIONS': {
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'SOCKET_CONNECT_TIMEOUT': 5,
            'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
            'CONNECTION_POOL_CLASS_KWARGS': {
                'max_connections': 50,
                'timeout': 20,
            },
            'PICKLE_VERSION': 2,
        }
    }
}

IMAGE_UPLOAD_PATH = '/var/image/image_upload/'
POS_URL = 'https://preprod.treebo.com/api/v1/external/hotelads/updateposfile.xml'
# Third party configs
TRIVAGO_CSV_LINK = '/var/data/trivago/trivago.csv'
TRIVAGO_BID_FILE_PATH = '/var/data/trivago/'

TRANSACTION_URL = "https://preprod.treebo.com/api/v1/external/hotelads/transactionmessage.xml"
GHA_CSV_EMAIL_ID = ['websales@treebohotels.com']

# GHA Updated Files
FEATURE_POS_MAIL = True
TOOLS_TRANSACTION_URL = "http://tools.treebohotels.com/api/v1/external/hotelads/transactionmessage.xml"
TOOLS_POS_URL = 'http://tools.treebohotels.com/api/v1/external/hotelads/updateposfile.xml'
POS_EMAIL = ["websales@treebohotels.com"]
SAVE_EMAIL_API_FAILURE_EMAIL_ID = ["web@treebohotels.com"]

# trip advisor
TA_CSV_EMAIL_ID = ["websales@treebohotels.com"]

# criteo
CRITEO_CSV_EMAIL_ID = ["websales@treebohotels.com"]

# for coupons
TREEBO_BASE_REST_HOST = 'http://tools.treebohotels.com'
SITE_HOST_NAME = TREEBO_BASE_REST_HOST

S3_GENERATED_COUPONS_URL = "https://s3-ap-southeast-1.amazonaws.com/treebo/admin/generated_coupons/"

# growth url
GROWTH_ADD_HOTEL_URL = "http://growth.treebohotels.com/growth/common_service/update_hotel_info/"
# GHA
FETCH_BOOKING_STATUS = "http://growth.treebohotels.com/growth/transformer/booking/status/?wrid="

# for new hotels smtp
EMAIL_HOST_HOTEL_SMTP = 'smtp.gmail.com'
EMAIL_HOST_USER_HOTEL_SMTP = 'alerts.webbooking@treebohotels.com'
EMAIL_HOST_PASSWORD_HOTEL_SMTP = 'AdMinTreeBo@9932174'
EMAIL_PORT_HOTEL_SMTP = 465

HX_CONFIG = HX_PROD

# cancel hash urls
GROWTH_FETCH_BOOKING_DATA_URL = "http://growth.treebohotels.com/growth/cancellation/cancelbookingdetails/"
GROWTH_SUBMIT_BOOKING_URL = "http://growth.treebohotels.com/growth/cancellation/submitcancellationrequest/"
GROWTH_CANCEL_HASH_URL = "http://growth.treebohotels.com/growth/cancellation/cancelhash"
GROWTH_BOOKING_EXP_URL = "http://growth.treebohotels.com/growth/booking_exp/booking_exp_url/"

EASY_JOB_LITE_SETTINGS_PATH = os.path.join(
    EASY_JOB_LITE_SETTINGS_DIR, ENVIRONMENT + ".yaml")
AUTH_SERVER_HOST = 'http://auth.treebo.com'
APP_CRENDENTIALS = {
    'password': {
        'client_id': 'dkQzsPZDU83jSpZeZ84WGbenu2UQ9H0WGfvy5lFW',
        'client_secret':
            'svjozF4pOa7iq6JnPe1nKtdteb0N30aJgqRzkIdO6QTYUl3SZgSBDbVORJTDfxxnOAtlS1iHD94dhEnU7ZWMhk3CEOCjtwD8YNOlVH8PHAyGY0yvU8mkZvMxMf1WwyFB'
    }
}
PROWL_HOTEL_POSITIVES_DATA_URL = "http://api.treebohotels.com/prowl/rest/v3/hotels/positivereviews/?hotelogix_code={0}&days={1}"
PAYMENT_SERVICE_HOST = 'https://payment.treebo.com/api'

NRP_ENABLED = True
# seo phase 2
GROWTH_FREQUENTLY_BOOKED_URL = 'http://growth.treebohotels.com/growth/transformer/frequently_booked/'


TRIPADVISOR_BASE_URL = "https://www.tripadvisor.com/"
AB_SETTINGS = {
    'is_ab_activated': True,
    'ab_base_url': 'http://ab.treebo.com',
    'application_id': 'website',
    'cookies_enabled': False
}

HOTEL_POLICIES_EMAILS = [
    'ge@treebohotels.com',
    'gdcteam@treebohotels.com',
    'guest.delight@treebohotels.com']

CATALOGUE_SERVICE_RABBITMQ_SETTINGS = {
    'name': 'catalogue_service',
    'url': 'amqp://cmuser:M3b00@shared-rmq.treebo.com:5672/catalog',
    'exchange': 'cs_exchange',
    'routing_key': 'com.cs.property',
}

CATALOGUING_SERVICE_BASE_URL = "http://catalog.treebo.com/cataloging-service/"

TREEBO_BASE_HOST_URL = 'gdc.treebohotels.com'


TREEBO_WEB_URL = "https://www.treebo.com"
TREEBO_BASE_URL = '//' + TREEBO_BASE_HOST_URL
TREEBO_BASE_REST_HOST = TREEBO_WEB_URL



PROWL_HOTEL_DISABLE = 'http://api.treebohotels.com/prowl/rest/v1/hotels/disable'

AVAILABILITY_HOOK_API = {
    'b2b_axis_room_hook_api': "http://corp-api.treebo.com/ext/api/daywiseInventory/"
}


DIRECT_TEAM = [
    'bibek.padhy@treebohotels.com',
    'varun.achar@treebohotels.com',
    'amitakhya.bhuyan@treebohotels.com',
    'gaurav.kinra@treebohotels.com',
    'aditya.shetty@treebohotels.com',
    'kapil.matani@treebohotels.com',
    'arun.midha@treebohotels.com',
    'aishwary.varshney@treebohotels.com',
    'rumani.kumari@treebohotels.com',
    'subidita.chaki@treebohotels.com',
    'vivek.gupta@treebohotels.com',
    'subham.das@treebohotels.com',
]

CANCELLATION_FAILURE_EMAIL_LIST = [] + DIRECT_TEAM

RESERVATION_FAILURE_EMAIL_LIST = [] + DIRECT_TEAM

RACK_RATE_FAILURE = [] + DIRECT_TEAM

LOG_PATH_VARIABLE = 'LOG_PATH'

PDF_URL = "http:{0}{1}?order_id={2}&pretax_price={3}&total_tax={4}"

CRS_INTEGRATION_EVENT_RABBITMQ_SETTING = {
    'name': 'crs_integration',
    'url': 'amqp://web:M3b00@shared-rmq.treebo.com:5672/web',
    'exchange': 'crs_integration_event_exchange_tools',
    'routing_key': 'hotel.migrated',
}
CRS_INTEGRATION_EVENT_QUEUE_NAME = 'crs_integration_event_queue_tools'

CS_QUEUE_NAME = 'cs_queue_tools'

HEALTH_CHECK_CONF = dict(rmq_host=BROKER_URL, sqs_queue_name='queue_name', region_name='eu-west-1',
                         aws_secret_access_key='mykey', aws_access_key_id='access_id',
                         use_basic_db_check=True,
                         soft_dependencies=['CACHEHealthCheck', 'RMQHealthCheck']
                         )

# cataloguing apis
CATALOG_HOTEL_URL = 'http://catalog.treebo.com/cataloging-service/properties'
CATALOG_ROOMS_URL = 'http://catalog.treebo.com/cataloging-service/properties/{0}/rooms/'
CATALOG_HOTEL_FACILITIES_URL = 'http://catalog.treebo.com/cataloging-service/properties/{0}/amenities/'
CATALOG_ROOM_FACILITIES_URL = 'http://catalog.treebo.com/cataloging-service/properties/{0}/rooms/{1}/amenities/'
CATALOG_BANQUET_HALL_URL = 'http://catalog.treebo.com/cataloging-service/properties/{0}/banquet-halls/'
CATALOG_RESTAURANTS_URL = 'http://catalog.treebo.com/cataloging-service/properties/{0}/restaurants/'
CATALOG_BARS_URL = 'http://catalog.treebo.com/cataloging-service/properties/{0}/bars/'


GROWTH_BROKER_URL = 'amqp://growth:M3b00@shared-rmq.treebo.com:5672/growth'
DOMAIN_EVENT_ROUTING_KEY_PREFIX = 'domain_rk_prod_'
GROWTH_EXCHANGE_NAME = 'growth_prod_direct_internal_exchange'
GROWTH_EVENT_TYPE_FOR_BOOKING_STATUS = 'FlaggedReasonEvent'

GOOGLE_RECAPTCHA_V3_SECRET_KEY = "6LesgYkUAAAAAAGYc-bOfURTOWbKFXG-PPQLMzfB"

DISCOUNT_URL = 'http://koopan.treebo.com/v1/discounts/featured'
COUPON_PRICING_URL = 'http://koopan.treebo.com/v1/discounts/{}/mark_as_applied'
DISCOUNT_HOST = 'http://koopan.treebo.com'
GROWTH_REALIZATION_API_HOST = "https://growth.treebohotels.com"
COMMUNICATION_API_HOST = "https://communication.treebo.com"