#!/usr/bin/env bash

docker build -t nginx . \
  && (docker rm -f nginx-base-new || true)

docker run \
  -d \
  --net="host" \
  -v /opt:/opt \
  --volumes-from cybertron-hotrod \
  --volumes-from cybertron-rodimus \
  --volumes-from app_direct_staging_treebo \
  -v /var/log/nginx:/var/log/nginx \
  --name nginx-base-new \
  nginx \
  nginx -g 'daemon off;'
