server {
  listen 13380;
  server_name talkies.treebo.com;

  location / {
    proxy_pass https://treebo-alpha-6e9k.squarespace.com;
  }
}
