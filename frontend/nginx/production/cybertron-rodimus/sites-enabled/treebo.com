server {
  listen 13380;
  server_name treebo.com;
  return 301 https://www.treebo.com$request_uri;
}

