server {
  listen 13380;
  server_name assets.treebo.com;

  access_log /var/log/nginx/assets.treebo.com.access.log;
  error_log /var/log/nginx/assets.treebo.com.error.log;

  location / {
    deny all;
  }

  location /rodimus/build {
    alias /usr/src/cybertron/cybertron-rodimus/build;
    allow all;
    gzip_static on;
    add_header Last-Modified "";
    add_header Cache-Control public;
    add_header 'Access-Control-Allow-Origin' '*';
    expires max;
  }
}

