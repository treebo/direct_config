server {
    listen          13380;
    server_name     careers.treebo.com;

    location / {
        proxy_pass https://s3-ap-southeast-1.amazonaws.com/treebo/static/careers/workable.html; 
    }

}
