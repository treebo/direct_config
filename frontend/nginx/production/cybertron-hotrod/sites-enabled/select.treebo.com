server {
    listen          13370;
    server_name     select.treebo.com;

    location / {
        proxy_pass https://treebo-alpha-lxzh.squarespace.com/;
        proxy_redirect https://select.treebo.com https://treebo-alpha-lxzh.squarespace.com/;
    }

}
