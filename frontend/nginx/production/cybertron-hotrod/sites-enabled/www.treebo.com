map_hash_bucket_size 256;
map $request_uri $new_uri {
    include /etc/nginx/oldnew.map; #or any file readable by nginx
}
server {
  listen 13370;
  server_name www.treebo.com gdc.treebohotels.com;
  access_log /var/log/nginx/access_json.log le_json;
  large_client_header_buffers 4 16k;
  include /etc/nginx/custom-conf/url_redirect.conf;

  if ($http_x_forwarded_proto != "https") {
    rewrite ^/(.*) https://www.treebo.com/$1 permanent;
  }
  # Start: Block DDoS
  if ($http_user_agent ~* "afollestad\/Bridge|okhttp\/3.9.0|Go-http-client") {
    return 403;
  }
  if ($http_referer ~* "smswala") {
    return 403;
  }
  if ($http_user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0") {
    return 403;
  }

  if ($http_referer ~* "https://www.treebo.com/api/v2/hotels") {
    return 403;
  }
  set $is_valid_request "";
  if ($http_user_agent = "android") {
    set $is_valid_request "A";
  }
  if ($http_referer = "-") {
    set $is_valid_request "${is_valid_request}B";
  }
  if ($http_referer = "") {
    set $is_valid_request "${is_valid_request}C";
  }
  if ($is_valid_request = "AB") {
    return 403;
  }
  if ($is_valid_request = "AC") {
    return 403;
  }
  # End: Stop DDoS
  set $mobile_redirect do_not_perform;
  set $ismobile 0;
  if ($new_uri) {
            return 301 $new_uri;
  }
  location = / {
    limit_req zone=website burst=5;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto https;
    proxy_set_header Host $http_host;
    proxy_set_header ISMOBILE $ismobile;
    proxy_connect_timeout 7200;
    proxy_send_timeout 7200;
    proxy_read_timeout 7200;
    proxy_buffer_size 128k;
    proxy_buffers 4 256k;
    proxy_busy_buffers_size 256k;
    send_timeout 7200;

    proxy_pass http://localhost:1337;
  }

  # Temp hack to allow google to stop crawling hotels-in-new-delhi/ and start crawling /hotels-in-delhi
  location = /hotels-in-new-delhi/ {
    rewrite ^/hotels-in-new-delhi/$ $scheme://$host/hotels-in-delhi/ redirect;
  }
  location ~ /fot/(login|login/){
        rewrite ^/fot/(login|login/) $scheme://$host/ permanent;
  }

  location ~ ^/(api\/web|security|payment-process|perfect-stay|health|find|cities|search|(?!.*blog).*(-in-|-near-)|itinerary|confirmation|login|signup|register|forgot-password|serviceWorker|manifest|faq|account/booking-history|c-|tripadvisor|rewards|introducing-rewards|feedback) {
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto https;
    proxy_set_header Host $http_host;
    proxy_set_header ISMOBILE $ismobile;
    proxy_connect_timeout 7200;
    proxy_send_timeout 7200;
    proxy_read_timeout 7200;
    proxy_buffer_size 128k;
    proxy_buffers 4 256k;
    proxy_busy_buffers_size 256k;
    send_timeout 7200;
    
    proxy_pass http://localhost:1337;
  }

  location ~ ^/(user/reset-password|account) {
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto https;
    proxy_set_header Host $http_host;
    proxy_set_header ISMOBILE $ismobile;
    proxy_connect_timeout 7200;
    proxy_send_timeout 7200;
    proxy_read_timeout 7200;
    proxy_buffer_size 128k;
    proxy_buffers 4 256k;
    proxy_busy_buffers_size 256k;
    send_timeout 7200;
    if ( !-f $request_filename ) {
      set $optproxy A;
    }
    if ( $mobile_redirect = perform) {
      set $optproxy "${optproxy}B";
    }
    if ( $optproxy = AB ) {
      proxy_pass http://localhost:1338;
      break;
    }
    #proxy_buffering off;
    if ( $optproxy = A ) {
      proxy_pass https://www.treebohotels.com;
      break;
    }
  }

  location /blog {
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_connect_timeout       7200;
    proxy_send_timeout          7200;
    proxy_read_timeout          7200;
    send_timeout                7200;
    proxy_pass https://www2.treebo.com/blog;
  }

#  location /api/v1/sms-booking {
#    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
#    proxy_pass http://preprod1.treebo.com;
#  }

  location /api {
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_pass https://www.treebohotels.com;
  }

  location / {
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_set_header ISMOBILE $ismobile;
    proxy_redirect off;
    proxy_pass https://www.treebohotels.com;
  }
  location /robots.txt {
     proxy_pass https://s3-ap-southeast-1.amazonaws.com/treebo/static/files/robots.txt;
  }
  
  location /BingSiteAuth.xml {
     proxy_pass https://s3-ap-southeast-1.amazonaws.com/treebo/static/files/BingSiteAuth.xml;
  }
}

