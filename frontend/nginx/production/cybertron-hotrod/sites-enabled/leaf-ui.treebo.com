server {
  listen 13370;
  server_name leaf-ui.treebo.com;

  if ($http_x_forwarded_proto != "https") {
    rewrite ^/(.*) https://leaf-ui.treebo.com/$1 permanent;
  }

  location / {
    proxy_pass https://treebohotels.github.io/leaf-ui/;
  }
}
