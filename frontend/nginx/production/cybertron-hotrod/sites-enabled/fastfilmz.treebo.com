server {
  listen 13370;
  server_name fastfilmz.treebo.com;

  location / {
    proxy_pass https://treebo-alpha-6e9k.squarespace.com/new-page;
  }
}
