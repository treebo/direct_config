server {
  listen 13370;
  server_name blog.treebo.com; 
  return 301 https://www.treebo.com/blog$request_uri;
}

